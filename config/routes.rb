Pos::Application.routes.draw do

  get "roles_permissions/index"

  get "roles_permissions/show"

  get "roles_permissions/create"

  get "product_categories/edit"

  get "products/edit"

  get "roles/edit"

  resources :reports, :only => [:index, :create, :update, :delete] do
    collection do
      get "models"
      post "run"
      get "designer"
    end

    member do

    end
  end

  resources :partials, :only => [:index]

  resources :permissions, :only => [:index, :create]

  resources :roles, :only => [:index, :new, :edit]

  resources :users, :only => [:index]

  resources :product_categories, :only => [:index, :new ,:show]

  resources :products, :only => [:index, :show, :new]

  resources :purchase_orders, :only => [:index, :new, :show]

  resources :purchases , :only => [:index, :new, :show]
 
  resources :sales, :only => [:index , :new, :show]

  resources :sales, :only => [:index , :new, :show]
  
  get "home/index"

  get "print/report"

  scope "print/record" do
    get "sales/show"
    get "purchases/show"
    get "purchase_orders/show"
  end

  get "print/record" 

  # application routes go into an api

  scope "print/record/" do
    namespace :api , defaults: {:format => :json } do
      resources :users
      resources :products 
      resources :product_categories
      resources :purchase_orders
      resources :purchases
      resources :sales
      resources :recent_operations, only: [:index]
      get "sales_by_users", to: "dashboard#sales_by_users"
      get "most_popular_products", to: "dashboard#most_popular_products"
      # get "recent_operations/index", to: "recent_operations#index"
    end    
  end

  namespace :api , defaults: {:format => :json } do
    resources :user_roles, :only => [:index, :create]
    resources :roles do
      resources :permissions_roles
    end
    resources :permissions, :only => [:index]
    resources :users, :only => [:index, :show, :edit, :update]
    resources :products 
    resources :product_categories
    resources :purchase_orders
    resources :purchases
    resources :sales
    resources :recent_operations, only: [:index]
    
    get "sales_by_users", to: "dashboard#sales_by_users"
    get "most_popular_products", to: "dashboard#most_popular_products"
    # get "recent_operations/index", to: "recent_operations#index"
  end   

  devise_for :users

  devise_scope :user do
    get 'register', to: "devise/registrations#new", as: :register
    get "login", to: "devise/sessions#new", as: :login
    get "logout", to: "devise/sessions#destroy", as: :logout
  end

  # overrides
  get "users/default/edit", :to => "users#edit"

  root to: "application#index"
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end

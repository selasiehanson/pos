class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :name
      t.string :query_string
      t.text :query_object
      t.integer :created_by_id
      t.integer :updated_by_id

      t.timestamps
    end
  end
end

class CreatePurchaseOrders < ActiveRecord::Migration
  def change
    create_table :purchase_orders do |t|
      t.datetime :order_date
      t.string :order_id
      t.integer :created_by_id
      t.integer :updated_by_id

      t.timestamps
    end
  end
end

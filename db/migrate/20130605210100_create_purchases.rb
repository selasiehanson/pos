class CreatePurchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.datetime :purchase_date
      t.string :purchase_id
      t.integer :purchase_order_id
      t.integer :purchase_by_id
      t.integer :received_by_id
      t.integer :created_by_id
      t.integer :updated_by_id

      t.timestamps
    end
  end
end

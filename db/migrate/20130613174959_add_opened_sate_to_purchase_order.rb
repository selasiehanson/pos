class AddOpenedSateToPurchaseOrder < ActiveRecord::Migration
  def change
    add_column :purchase_orders, :opened, :boolean, default: true
  end
end

class CreatePurchaseOrderItems < ActiveRecord::Migration
  def change
    create_table :purchase_order_items do |t|
      t.references :purchase_order
      t.references :product
      t.integer :created_by_id
      t.integer :updated_by_id

      t.timestamps
    end
    add_index :purchase_order_items, :purchase_order_id
    add_index :purchase_order_items, :product_id
  end
end

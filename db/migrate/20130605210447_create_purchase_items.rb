class CreatePurchaseItems < ActiveRecord::Migration
  def change
    create_table :purchase_items do |t|
      t.integer :purchase_id
      t.integer :product_id
      t.integer :quantity
      t.decimal :unit_cost, :precision => 10, :scale => 2
      t.decimal :total_cost, :precision => 10, :scale => 2
      t.integer :supplier_id
      t.integer :created_by_id
      t.integer :updated_by_id

      t.timestamps
    end
  end
end

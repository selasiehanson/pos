class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.string :customer
      t.decimal :amount_collected, :precision => 8, :scale => 2
      t.decimal :total_amount, :precision => 8, :scale => 2
      t.decimal :change, :precision => 8, :scale => 2
      t.integer :discount
      t.integer :created_by_id
      t.integer :updated_by_id
      t.timestamps
    end
  end
end

class AddQuantityToPurchaseOrderItem < ActiveRecord::Migration
  def change
    add_column :purchase_order_items, :quantity, :integer
  end
end

class CreatePermissionsRoles < ActiveRecord::Migration
  def change
    create_table :permissions_roles do |t|
      t.belongs_to :role
      t.belongs_to :permission

      t.timestamps
    end
    add_index :permissions_roles, :role_id
    add_index :permissions_roles, :permission_id
  end
end

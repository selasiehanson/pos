class CreateSaleItems < ActiveRecord::Migration
  def change
    create_table :sale_items do |t|
      t.references :product
      t.references :sale
      t.integer :quantity
      t.decimal :selling_price, :precision => 8, :scale => 2
      t.decimal :line_total, :precision => 8, :scale => 2
      t.integer :created_by_id 
      t.integer :updated_by_id
      t.timestamps
    end
    add_index :sale_items, :product_id
  end
end

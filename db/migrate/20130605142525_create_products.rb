class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.integer :quantity
      t.integer :reorder_quantity
      t.integer :selling_price, precision: 10, scale: 2
      t.integer :created_by_id
      t.integer :updated_by_id
      t.integer :product_category_id
      t.timestamps
    end
  end
end

module	DesignHandler
	
	class Push
		
		def initialize
			
		end
		
		def app_models
			out = ActiveRecord::Base.connection.tables.map do |table|
			  table_name = get_klass_string(table)
			  unless table_name == "SchemaMigration"
			  	_model = get_klass(table)
			  	assocs = _model.reflections.keys
			  	associations = assocs.map do |assoc|
			  		{
			  			table_name: _model.reflections[assoc].table_name,
			  			name: assoc
			  		}
			  	end

			  	{ 
			  		name: table_name,
			  		table_name: table,
			  		fields: _model.columns.map { |col| { name: col.name , type: col.type}}, 
			  		associations: associations
			  	}
				end
			end
			out.compact
		end

		def run_query(params)
	  	select = params[:select]
	  	models = params[:models].keys
	    group = params[:groups]
	  	aggregates = params[:aggregates]

	    model = models[0]
	  	_joins = {}
	    _group = group[:test] || {}

	    join_holder = params[:joins] || []
	    
	    unless join_holder.empty?
	      if join_holder.size == 1
	        _joins = join_holder[0].to_sym
	      else
	        convert_array_to_nested_hash(join_holder,_joins,{},join_holder[0], false)
	      end
	    end
	  	
			res = parse_fields(select,_group,aggregates)

	   	sql = get_sql(model, res[:select], params[:is_test], _joins, _group)
	  	results = ActiveRecord::Base.connection.execute(sql)
	  	
	  	{
	  		query_fields: res[:select],
	  		sql: sql,
	  		data: {
	  			fields: res[:incoming_fields].map {|field| { key: field, value: field.humanize } },
	  			results: results	
	  		}
	  	}
  	end

		private 
			def parse_fields(select, _group, aggregates)
				incoming_fields = []
				_select = []
				select.each do |model, _fields|
		  		table_name = get_model(model).table_name
		  		resolved_fields =	_fields.map do |field| 
		  			if aggregates.keys.include?(model) and aggregates[model.to_s][field.to_s] != nil
		          val = aggregates[model.to_s][field.to_s]
		          header = "#{table_name}_#{field}_#{val}"
		          
		          agg_expression = "#{val}(#{table_name}.#{field})"  
		          pre_expression = "#{table_name}.#{field}"
		          if _group.include?(pre_expression)
		            idx = _group.index(pre_expression)
		            _group[idx] = "#{agg_expression}"  
		          end

		          str = " #{agg_expression} as #{header}" 
		          incoming_fields << header  
		        else
		          header = "#{table_name}_#{field}"
		          str =  "#{table_name}.#{field} as #{header}" 
		          incoming_fields << header
		        end		        
		  			str
		  		end

		  		resolved_fields = resolved_fields.join(", ")
		  		_select << resolved_fields
		  	end
		  	{
		  		select: _select,
		  		incoming_fields:  incoming_fields
		  	}
			end

			def get_sql(model, _select, is_test = true,_joins = {}, _group = [], where = [])
				sql = get_model(model).select(_select).joins(_joins)
		    if is_test
		      sql = sql.group(_group)
		    end
		    sql = sql.to_sql
			end

	    def convert_array_to_nested_hash(arr, holder,prev,item,last)
	      is_last = false
	      if (arr.index(item) == arr.size - 1)
	        is_last = true
	        val = item
	      else
	        val = {}
	      end

	      if is_last
	        idx = arr[arr.index(item) - 1].to_sym
	        prev[idx] = item.to_sym
	      else
	        next_index = arr.index(item) + 1
	        key = item.to_sym
	        holder[key] = val
	        convert_array_to_nested_hash(arr, holder[key], holder,arr[next_index], last)
	      end
	    end

	  	# model comes as a String
	  	def get_model(model)
	  		model.constantize
	  	end

	  	def get_klass_string(table)
	  		table.capitalize.singularize.camelize
	  	end

	  	def get_klass(table)
	  		table.capitalize.singularize.camelize.safe_constantize
	  	end

		  def process_query()
		  	
		  end
		
	end	
end


namespace :permissions do
  desc "TODO"
  task :create => :environment do
  	arr = []
    #load all the controllers
    controllers = Dir.new("#{Rails.root}/app/controllers").entries
    controllers.each do |entry|
      if entry =~ /_controller/
        #check if the controller is valid
        arr << entry.camelize.gsub('.rb', '').constantize
      elsif entry =~ /^[a-z]*$/ #namescoped controllers
        Dir.new("#{Rails.root}/app/controllers/#{entry}").entries.each do |x|
          if x =~ /_controller/
            arr << "#{entry.titleize}::#{x.camelize.gsub('.rb', '')}".constantize
          end
        end
      end
    end

    arr.each do |controller|
    	if controller.permission
    		# we create an initial permission that allows us to manage all other actions
    		write_permission(controller.permission, "manage", "manage all actions")
    		controller.action_methods.each do |method|
    			if method =~ /^([A-Za-z\d*]+)+([\w]*)+([A-Za-z\d*]+)$/
    				description, cancan_action = eval_cancan_action(method)
    				write_permission(controller.permission, cancan_action, description)
    			end
    		end
    	end
    end
    puts "Done"

  end

  def eval_cancan_action(action)
	  case action.to_s
	  when "index", "show", "search"
	    cancan_action = "read"
	    action_desc = I18n.t :read
	  when "create", "new"
	    cancan_action = "create"
	    action_desc = I18n.t :create
	  when "edit", "update"
	    cancan_action = "update"
	    action_desc = I18n.t :edit
	  when "delete", "destroy"
	    cancan_action = "delete"
	    action_desc = I18n.t :delete
	  else
	    cancan_action = action.to_s
	    action_desc = "Other: " << cancan_action
	  end

	  return action_desc, cancan_action
  end

  def write_permission(model, cancan_action, description)
  	permission = Permission.find(:first, :conditions => ["subject_class = ? AND action = ?", model, cancan_action])
  	unless permission
  		permission = Permission.new
  		permission.subject_class = model
  		permission.action = cancan_action
  		permission.description =  description
  		permission.save
  	end
  end
  
end

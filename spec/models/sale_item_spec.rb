require 'spec_helper'

describe SaleItem do
	before do
		@sale_item = FactoryGirl.build(:sale_item)
	end

	subject {@sale_item}
	describe "properties" do
		it { should respond_to(:product_id) }
		# it { should respond_to(:sale_id) }
		it { should respond_to(:quantity) }
		it { should respond_to(:selling_price) }
		it { should respond_to(:line_total) }
		it { should respond_to(:created_by_id) }
		it { should respond_to(:updated_by_id) }
	end

	describe "Associations" do
		it { should respond_to(:product) }
		it { should respond_to(:sale) }
		it { should respond_to(:created_by) }
		it { should respond_to(:updated_by) } 
	end

	describe "Validations" do
		[:product_id, :created_by_id, :updated_by_id].each do |attr|
			it "requires #{attr}" do
				subject.send("#{attr}=", nil)
				subject.should_not be_valid
				subject.errors[attr].should_not be_nil
			end
		end

		[:quantity, :selling_price].each do |attr|
			it "#{attr} should be greater than 0" do
				subject.send("#{attr}=", 0)
				subject.should_not be_valid
				subject.errors[attr].should_not be_nil
			end
		end
	end

	describe "After save hooks" do
		it "should have a line_total after saving" do
			subject.quantity = 5
			subject.save
			subject.reload
			subject.line_total.should == subject.quantity * subject.selling_price
		end
	end

	describe "other methods" do
		
		context "most popular n products" do
			before do
				@p1 = FactoryGirl::create(:product)
				@p2 = FactoryGirl::create(:product)
				FactoryGirl::create(:sale_item, product_id: @p1.id)
				FactoryGirl::create(:sale_item, product_id: @p1.id)
				FactoryGirl::create(:sale_item, product_id: @p1.id)
				FactoryGirl::create(:sale_item, product_id: @p1.id)
				FactoryGirl::create(:sale_item, product_id: @p1.id)

				FactoryGirl::create(:sale_item, product_id: @p2.id)
				FactoryGirl::create(:sale_item, product_id: @p2.id)
			end

			it "should return the product name and highest quantity sold" do
				n = 2
				products = SaleItem.most_popular_n_products(n)
				products.length.should == 2
				products[0].should respond_to(:product_name)
				products[0].should respond_to(:quantity)
			end

			it "should return the highest first" do
				n = 2
				products = SaleItem.most_popular_n_products(n)
				products.length.should == 2
				products[0].product_id.should == @p1.id
				products[1].product_id.should == @p2.id
				products[0].quantity.should > products[1].quantity
			end
		end

	end
end
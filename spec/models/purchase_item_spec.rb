require 'spec_helper'

describe PurchaseItem do

	before do
		@purchase_item = FactoryGirl.create(:purchase_item)
	end

	subject { @purchase_item }

	describe "Properties" do
		it { should respond_to(:purchase_id) }
		it { should respond_to(:product_id) }
		it { should respond_to(:quantity) }
		it { should respond_to(:unit_cost) }
		it { should respond_to(:total_cost) }
		it { should respond_to(:supplier_id) }
		it { should respond_to(:created_by_id) }
		it { should respond_to(:updated_by_id) }
	end

	describe "Associations" do
		it { should respond_to(:product) }
		it { should respond_to(:purchase) }
		it { should respond_to(:created_by) }
		it { should respond_to(:updated_by) }
	end

	describe "Validations" do
		[:product_id, :quantity, :unit_cost, :total_cost, :created_by_id, :updated_by_id].each do |attr|
			it "requires #{attr}" do
				subject.send("#{attr}=", nil)
				subject.should_not be_valid
				subject.errors.should_not be_nil
			end
		end

		[:quantity, :unit_cost, :total_cost].each do |attr|
			it "cannot have #{attr} to be negative " do
				subject.send("#{attr}=",-2)
				subject.should_not be_valid
				subject.errors.should_not be_nil
			end
		end
	end

end

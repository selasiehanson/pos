require 'spec_helper'

describe PurchaseOrderItem do
	
	before do
		@poi = FactoryGirl.create(:purchase_order_item)
	end

	subject { @poi }

	describe "Properties" do
		it { should respond_to(:purchase_order_id) }
		it { should respond_to(:product_id) }
		it { should respond_to(:quantity) }
		it { should respond_to(:created_by_id) }
		it { should respond_to(:updated_by_id) }
	end

	describe "Associations" do
		it { should respond_to(:product) }
		it { should respond_to(:purchase_order) }
	end

	describe "Validations" do
		[:product_id, :created_by_id, :updated_by_id].each do |attr|
			it "requires #{attr}" do
				subject.send("#{attr}=", nil)
				subject.should_not be_valid
				subject.errors.should_not be_nil
			end

			it "quantity should have a minimum value of 1" do
				@poi.quantity = 0
				subject.should_not be_valid
				subject.errors[:quantity].should_not be_nil
			end
		end
	end
end

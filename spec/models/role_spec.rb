require 'spec_helper'

describe Role do
	before	 do
		@role = FactoryGirl.create(:role)
	end

	subject { @role }
  describe "properties" do
  	it {should respond_to(:name) }
  end

  describe "Associations" do
  	# it { should respond_to(:user) }
    it { should respond_to(:permissions) }
  end
end

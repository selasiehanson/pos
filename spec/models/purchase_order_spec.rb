require 'spec_helper'

describe PurchaseOrder do

	before do
		@purchase_order = FactoryGirl.create(:purchase_order)
	end
	
	subject { @purchase_order }

	describe "Properties" do
		it { should respond_to(:order_date)}	
		it { should respond_to(:order_id) }
		it { should respond_to(:created_by_id)}
		it { should respond_to(:updated_by_id)}
		it { should respond_to(:opened) }
	end

	describe "Associations" do
		it { should respond_to(:purchase) }
		it { should respond_to(:purchase_order_items)}
		it { should respond_to(:created_by)}
		it { should respond_to(:updated_by)}
	end

	describe "Validations" do
		[:created_by_id, :updated_by_id].each do |attr| 
			it "requires #{attr}" do
				subject.send("#{attr}=", nil)
				subject.should_not be_valid
				subject.errors.should_not be_nil
			end
		end
	end

	describe "Other methods" do
		before do
			@p1 = FactoryGirl.create(:purchase_order)
			@p2 = FactoryGirl.create(:purchase_order, opened: false)
			
		end
		it "should ony return orders that are closed" do
			PurchaseOrder.closed.should == [@p2]
		end

		it "should ony return orders that are opened" do
			PurchaseOrder.open_orders.should == [@p1, @purchase_order]
		end
	end
end

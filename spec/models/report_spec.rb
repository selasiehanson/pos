require 'spec_helper'

describe Report do
  before do
  	@report =  FactoryGirl.create(:report)
  end
  subject { @report }

  describe	"properties" do
  	it { should respond_to(:name) }
  	it { should respond_to(:query_string)  }
		it { should respond_to(:query_object)  }
		it { should respond_to(:created_by_id) }
		it { should respond_to(:updated_by_id) }
  end

  describe "Validations" do
    [:name].each do |attr|
      it "should not have #{attr} as blank" do
        subject.send("#{attr}=", nil)
        subject.should_not be_valid
        subject.errors[attr].should_not be_nil
      end
    end

    it "should have a unique name" do
      rep = FactoryGirl.build(:report, name: "some report")
      rep.should_not be_valid
    end
  end
end

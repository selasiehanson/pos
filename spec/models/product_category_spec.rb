require 'spec_helper'

describe ProductCategory do
	before do
		@product_category = FactoryGirl.create(:product_category)
	end

	subject { @product_category }

	describe "properties" do
		it { should respond_to (:name) }
		it { should respond_to (:description) }
		it { should respond_to(:created_by_id) }
 		it { should respond_to(:updated_by_id) }
	end

	describe "associations" do
		it { should respond_to(:created_by) }
 		it { should respond_to(:updated_by) }
		it { should respond_to(:products) }
	end

	describe "validations" do
		
		[:name, :created_by_id, :updated_by_id].each do |attr|
			it "requires #{attr}" do
				subject.send("#{attr}=", nil)
				subject.should_not be_valid
				subject.errors[attr].should_not be_nil
			end
		end
	end
end

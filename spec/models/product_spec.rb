require 'spec_helper'

describe Product do
  before do
  	@product = FactoryGirl.create(:product)
  end

  subject { @product }

 	describe "properties" do
 		it { should respond_to(:name) }	
 		it { should respond_to(:description) }	
 		it { should respond_to(:quantity) }	
 		it { should respond_to(:reorder_quantity) }	
 		it { should respond_to(:selling_price) }	
 		it { should respond_to(:product_category_id) }
 		it { should respond_to(:created_by_id) }
 		it { should respond_to(:updated_by_id) }
 	end

 	describe "associations" do 
 		it { should respond_to(:created_by) }
 		it { should respond_to(:updated_by) }
 		it { should respond_to(:product_category) }
 		it { should respond_to(:purchase_order_items)}
 		it { should respond_to(:purchase_items)}
 	end

 	describe "validations" do |attr|
		[:name, :quantity, :reorder_quantity, :selling_price, 
			:product_category_id, :created_by_id, :updated_by_id].each do | attr|
			it "requires #{attr}" do
	 			subject.send("#{attr}=",nil)
	 			subject.should_not be_valid
	 			subject.errors[attr].should_not be_nil
	 		end
	 	end

	 	[:quantity, :reorder_quantity].each do |attr|
	 		it "#{attr} should have a minimum value of 0" do
	 			subject.send("#{attr}=", -2)
	 			subject.should_not be_valid
	 			subject.errors[attr].should_not be_nil
	 		end
	 	end

	 	it "should not have a negative selling_price" do
	 		@product.selling_price = -9.00
	 		subject.should_not  be_valid
	 		subject.errors[:selling_price].to_sentence.should eq "must be greater than 0.0"
	 	end
 	end

 	describe "Other methods" do
 		
 		describe "product viable to be sold" do
 			before	do
 				@p1 = FactoryGirl.create(:product, quantity: 0)
 				@p2 = FactoryGirl.create(:product, quantity: 10)
 			end
 			it "should return product with quantities > 0" do
 				Product.sales_items.should == [@product, @p2]
 			end
 		end
 	end
end

require 'spec_helper'

describe Purchase do
	before do
		@purchase = FactoryGirl.create(:purchase)
	end

	subject { @purchase }

	describe "properties" do
		it { should respond_to(:created_by_id)}
		it { should respond_to(:updated_by_id)}
		it { should respond_to(:purchase_id) }
		it { should respond_to(:purchase_order_id)}	
		it { should respond_to(:purchase_by_id)}
		it { should respond_to(:received_by_id)}
		it { should respond_to(:purchase_date)}
	end

	describe "Validations" do
		[:created_by_id, :updated_by_id, :purchase_order_id,
		:purchase_by_id, :received_by_id].each do |attr|
			it "requires #{attr}" do
				subject.send("#{attr}=", nil)
				subject.should_not be_valid
				subject.errors.should_not be_nil
			end
		end

		it "should assign a purchase date and id before it saves the record" do
			@purchase.purchase_date = nil
			@purchase.purchase_id = nil
			@purchase.save
			@purchase.reload
			@purchase.purchase_date.should_not be_nil
			@purchase.purchase_id.should_not be_nil
		end
	end

	describe "associations" do
		it { should respond_to(:created_by)}
		it { should respond_to(:updated_by)}
		it { should respond_to(:purchase_order)}	
		it { should respond_to(:purchase_by)}
		it { should respond_to(:received_by)}
	end
end

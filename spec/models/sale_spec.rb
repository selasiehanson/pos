



require 'spec_helper'

describe Sale do
	before do
		@user1 = FactoryGirl::create(:user)
		@user2 = FactoryGirl::create(:user, first_name: "Peter")
		@sale = FactoryGirl.create(:sale, created_by: @user1, updated_by: @user1)
	end

	subject { @sale }

	describe "properties" do
		it { should respond_to(:customer) }
		it { should respond_to(:amount_collected) }
		it { should respond_to(:total_amount) }
		it { should respond_to(:change) }
		it { should respond_to(:discount) }
		it { should respond_to(:created_by_id) }
		it { should respond_to(:updated_by_id) }
		# it { should respond_to(:sale_items_attributes) }
	end

	describe "Associations" do
		it { should respond_to(:created_by) }
		it { should respond_to(:updated_by) }
		it { should respond_to(:sale_items) }
	end

	describe "Validations" do
		[:created_by_id, :updated_by_id].each do |attr|
			it "requries #{attr} to be present" do
				subject.send("#{attr}=", nil)
				subject.should_not be_valid
				subject.errors[attr].should_not be_nil
			end
		end

		[:amount_collected, :total_amount].each do |attr|
			it "#{attr} should be greater than 0.00" do
				subject.send("#{attr}=",-0.1)
				subject.should_not be_valid
				subject.errors[attr].should_not be_nil
			end
		end

		it "change should be min value of 0.0" do
			@sale.change = -0.1
			subject.should_not be_valid
			@sale.change = 0
			subject.should be_valid
		end

		describe "discount should be between 0 and 100" do
			it "should have min discount value of between 0" do
				@sale.discount = -0.1
				subject.should_not be_valid
			end

			it "should have max discount value of 100" do
				@sale.discount = 101
				subject.should_not be_valid
			end
		end		
	end

	describe "other methods" do
			
		context "retrieving totals sales by users" do
			before do
				@sale1 = FactoryGirl::create(:sale, created_by: @user1, updated_by: @user1)
				@sale2 = FactoryGirl::create(:sale, created_by: @user1, updated_by: @user1)
				@sale3  =  FactoryGirl::create(:sale, created_by: @user2, updated_by: @user2)
			end

			it "should return a list of users and the total amount in sales and total number of items sold" do
				totals = Sale.total_sales_by_users
				totals.length.should == 2
				first = totals.first
				first.should respond_to(:total_amount)
				first.should respond_to(:sales_count)

			end	
			it "should return the highest first and lowest last in terms of total amount"  do
				totals = Sale.total_sales_by_users
				# we do the comparisons by user id instead since we do  not return the entire user object
				totals[0].created_by_id.should == @user1.id
				totals[1].created_by_id.should == @user2.id
				totals[0].total_amount.should be > totals[1].total_amount
			end
		end
		
	end

end

require 'spec_helper'

describe User do
	before  do
		@user = FactoryGirl.create(:user)
	end

	subject { @user }
	describe "properties" do
	 	it { should respond_to(:first_name) }
	 	it { should respond_to(:last_name) }
	 	it { should respond_to(:user_name) }   
	 	it { should respond_to(:email) }
	 	it { should respond_to(:admin) }
	end

	describe "Assocications" do
		it { should respond_to(:created_products) }
		it { should respond_to(:updated_products) }
		it { should respond_to(:roles) }
	end
	describe "validations" do
		[:first_name, :last_name,:user_name].each  do |attr|
			it "requires #{attr}" do
				subject.send("#{attr}=", nil)
				subject.should_not be_valid
				subject.errors.should_not be_nil
			end
		end
	end
end

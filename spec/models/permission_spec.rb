require 'spec_helper'

describe Permission do
	before do
		@permission = FactoryGirl.create(:permission)
	end
	
	subject { @permission } 

 	describe "Propeties" do
 		it { should respond_to(:subject_class) }
 		it { should respond_to(:action) }
 	end
end

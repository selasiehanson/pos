FactoryGirl.define do 

	factory :permission  do 
		subject_class "User"
		action "manage"
	end

	factory :role do
		name "sales"
		# ignore do
		# 	user
		# end
		# users { [user] }
		# factory :role_with_permissions do
		# 	ignore do
		# 		for_models {[]}
		# 		models ["User", "Sale","ProductCategory","Product", "PurchaseOrder",
		# 			"Purchase","Role", "Permission"]
		# 	end	

		# 	after(:create) do |role,evaluator|
		# 		_model = evaluator.models & evaluator.for_models
		# 		create(:permission, subject_class: _model[0], role: role)
		# 	end
		# end		
	end

	factory :user , aliases: [:created_by, :updated_by, :purchase_by, :received_by] do
		first_name "Kweku"
		last_name "Manu"
		user_name "Kman"
		sequence :email do |n|
		  "person#{n}@example.com"
		end
		password "12345678"
		
		# ignore do
		# 	role_names []
		# end

		# factory :user_with_roles do
		# 	after(:create) do |user, evaluator| 
		# 		evaluator.role_names ||= []
		# 		unless evaluator.role_names.empty?
		# 			# user.roles << create(:role_with_permissions, for_models: ["Purchase"])
		# 			user.roles << create(:role, name: evaluator.role_names[0])
		# 		end
		# 	end
		# end
		
	end

	factory :product_category do
		name "groceries"
		created_by
		updated_by
	end

	factory :product do
		name "milk"
		quantity 1
		reorder_quantity 5
		selling_price 2.00
		product_category
		created_by
		updated_by
	end


	factory :purchase_order do	
		created_by
		updated_by
	end

	factory :purchase_order_item do
		purchase_order
		product
		quantity 1
		created_by
		updated_by
	end

	factory :purchase do
		purchase_date DateTime.now
		purchase_order
		purchase_by
		received_by
		created_by
		updated_by
	end

	factory :purchase_item do
		purchase
		product
		created_by
		updated_by
		total_cost 0.99
		unit_cost 0.99
		quantity 1
	end

	factory :sale do
		amount_collected 20.0
		change 2.00
		total_amount 18.00
		discount 0.00
		created_by 
		updated_by
	end

	factory :sale_item do
		product
		sale
		created_by
		updated_by
		quantity 1
		selling_price 1
	end

	factory :report do
		name "some report"
		query_string "select * from some table"
		query_object do
			{query: "some serialized"}
		end
		created_by
		updated_by
	end

end
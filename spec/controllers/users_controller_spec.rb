require 'spec_helper'

describe UsersController do

  before do
    @user = FactoryGirl.create(:user)
    role = FactoryGirl.create(:role, name: "users")
    permission = FactoryGirl.create(:permission, subject_class: "User", action: "read")
    @user.roles << role
    role.permissions << permission
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

end

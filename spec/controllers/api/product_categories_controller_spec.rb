require 'spec_helper'

describe Api::ProductCategoriesController do
	before do
		@user = FactoryGirl.create(:user)	
		role  = FactoryGirl.create(:role)
		permission = FactoryGirl.create(:permission, subject_class: "ProductCategory")
		@user.roles << role
		role.permissions << permission
		sign_in @user
	end

	describe "GET index" do
		it "returns the http success" do
			xhr :get, :index
			response.should be_success
		end
	end

	describe "Post create" do
		before	do
			@params = {
				product_category: {
					name: "some new category", 
					description: ""	
				}
			}
		end

		context "when succesful" do
			it "should ceate a new product category and it" do
				lambda { 
					xhr :post, :create, @params
				}.should change(ProductCategory, :count).by(1)
			end
		end
	end

	describe "POST 'update'" do
    context "succesful" do
      before do
        @new_product_category = FactoryGirl.create(:product_category)
      end
      it "should update the said product_category" do
        new_name ="XXX"
        put :update, id: @new_product_category, product_category: FactoryGirl.attributes_for(:product_category, :name => new_name)
        response.should be_success
        parsed_response = JSON.parse(response.body)
        parsed_response["data"]["name"].should == new_name
      end  
    end
    
  end
end

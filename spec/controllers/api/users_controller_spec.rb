require 'spec_helper'

describe Api::UsersController do
	# let!(:user) { FactoryGirl.create(:user) }
	before do
		@user = FactoryGirl.create(:user)	
		role  = FactoryGirl.create(:role)
		permission = FactoryGirl.create(:permission, subject_class: "User")
		@user.roles << role
		role.permissions << permission
		sign_in @user
	end

	describe "GET index" do
		before	 do
			FactoryGirl.create(:user)
		end

		it "should return all users" do
			xhr :get, :index
			response.should be_success
			parsed_json =  JSON.parse(response.body)
			parsed_json["total"].should == 2
		end
	end


	describe "GET show" do
		before	do
			@user = FactoryGirl.create(:user)
		end
		it "returns with success" do
			xhr :get, :show, :id => @user.id
			response.should be_success
			response.body.should_not be_nil 
		end
	end

end

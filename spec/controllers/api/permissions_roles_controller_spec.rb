require 'spec_helper'

describe Api::PermissionsRolesController do
  before do
  	@user = FactoryGirl.create(:user)	
		role  = FactoryGirl.create(:role)
		permission = FactoryGirl.create(:permission, subject_class: "PermissionsRole")
		@user.roles << role
		role.permissions << permission
		sign_in @user
  end

  describe "GET index" do
  	before	do
  		@role = FactoryGirl.create(:role)
  	end
  	
  	it "return http success" do
  		get :index, role_id: @role.id
  		response.should be_success
  	end
  end

  describe "POST create" do
  	before do
  		@role = FactoryGirl.create(:role)
  	end
  	it "creates new permissions for a particular role" do
  		# pending
  	end
  end

  describe "GET show" do
  	
  end
end

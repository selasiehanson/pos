require 'spec_helper'

describe Api::RecentOperationsController do
	let!(:user) { FactoryGirl::create(:user) }
	before	do
		sign_in user
	end
	describe "GET index" do	
		before do
			@product  = FactoryGirl::create(:product)
			@purchase_order = FactoryGirl.create(:purchase_order)
			@purchase = FactoryGirl::create(:purchase)
			@sale = FactoryGirl.create(:sale)
		end
		it "should return a list of key recent activities" do
			xhr :get, :index
			response.should be_success
		end
	end
end

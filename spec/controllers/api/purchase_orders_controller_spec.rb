require 'spec_helper'

describe Api::PurchaseOrdersController do

	before do
		@user = FactoryGirl.create(:user)	
		role  = FactoryGirl.create(:role)
		permission = FactoryGirl.create(:permission, subject_class: "PurchaseOrder")
		@user.roles << role
		role.permissions << permission
		sign_in @user
	end

	describe "Get index" do
		it "returns http success" do
			xhr :get, :index
			response.should be_success
		end

		it "should return data in a specific format" do
			xhr :get, :index
			response.body.should_not be_nil
		end

		describe "retrieving opened order" do	
			before do
				@p1 = FactoryGirl.create(:purchase_order)
				@p2 = FactoryGirl.create(:purchase_order)
				@p3 = FactoryGirl.create(:purchase_order, opened: false)
			end

			it "should return count of opened order" do
				xhr :get, :index, {status: "open"}
				parsed_json = JSON.parse(response.body)
				parsed_json["total"].should == 2
			end
		end

		describe "retrieving opened order" do	
			before do
				@p1 = FactoryGirl.create(:purchase_order, opened: false)
				@p2 = FactoryGirl.create(:purchase_order)
			end

			it "should return count of opened order" do
				xhr :get, :index, {status: "closed"}
				parsed_json = JSON.parse(response.body)
				parsed_json["total"].should == 1
			end
		end
	end

	describe "Get show" do
		before	do
			@purchase_order = FactoryGirl.create(:purchase_order)
		end
		it "returns with success" do
			xhr :get, :show, :id => @purchase_order.id
			response.should be_success
			response.body.should_not be_nil 
		end
	end

	describe "Post create" do
		before do
			# @user = FactoryGirl.create(:user)
			@purchase_order_params = {
				:purchase_order => {
					:order_date => "2013-06-12T00:00:00.000Z", 
				},
				:purchase_order_items =>[
					{
						:quantity => 4, 
						:product_id => 1
					}
				]	
			}
			# sign_in @user
		end

		context "when successfull" do
	        it "should create a purchase order" do
				lambda {
		          lambda {
		            post :create, @purchase_order_params
		          }.should change(PurchaseOrder, :count).by(1)  
		        }.should change(PurchaseOrderItem, :count).by(1)
			end

			before do
				post :create, @purchase_order_params
			end
			it "should bew successfull" do
				response.should be_success
				parse_json = JSON.parse(response.body)
				parse_json["success"].should == true
			end
		end

		context "when failure" do
			before do
				# @purchase_order_params
			end
			it "should NOT create a purchase order" do
				
			end
		end
		
	end
end

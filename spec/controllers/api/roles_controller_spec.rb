require 'spec_helper'

describe Api::RolesController do
  before do
    @user = FactoryGirl.create(:user)
    @role = FactoryGirl.create(:role)
    permission = FactoryGirl.create(:permission, subject_class: "User", action: "read")
    @user.roles << @role
    @role.permissions << permission
    sign_in @user
  end

  
  describe "GET 'index'" do
    it "returns http success" do
      xhr :get, :index
      response.should be_success
      response.body.should_not be_nil
    end
  end

  describe "GET 'show'" do
    before do
      
    end
    it "returns record for an individual role" do
      xhr :get, :show, :id => @role.id
      response.should be_success
      response.body.should_not be_nil
      parsed_json =  JSON.parse(response.body)
      parsed_json["data"]["id"].should == @role.id
    end
  end

  describe "GET 'create'" do
    before  do
      @params = {
        role: {
          name: "admin"
        }
      }
    end

    context "when succesful" do
      it "should ceate a new role and it" do
        lambda { 
          xhr :post, :create, @params
        }.should change(Role, :count).by(1)
      end
    end
  end

  describe "POST 'update'" do
    context "succesful" do
      before do
        @new_role = FactoryGirl.create(:role)
      end
      it "should update the said role" do
        new_name ="XXX"
        put :update, id: @new_role, role: FactoryGirl.attributes_for(:role, :name => new_name)
        response.should be_success
        parsed_response = JSON.parse(response.body)
        parsed_response["data"]["name"].should == new_name
      end  
    end
    
  end

  describe "GET 'delete'" do
    
  end

end

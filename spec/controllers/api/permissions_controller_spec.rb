require 'spec_helper'

describe Api::PermissionsController do

  before do
    @user = FactoryGirl.create(:user) 
    role  = FactoryGirl.create(:role)
    permission = FactoryGirl.create(:permission, subject_class: "Permissions")
    @user.roles << role
    role.permissions << permission
    sign_in @user
  end
  describe "GET 'index'" do
    it "returns http success" do
      xhr :get, :index
      response.should be_success
      response.body.should_not be_nil
    end
  end
end

require 'spec_helper'

describe Api::ProductsController do
	
	before do
		@user = FactoryGirl.create(:user)	
		role  = FactoryGirl.create(:role)
		permission = FactoryGirl.create(:permission, subject_class: "Product")
		@user.roles << role
		role.permissions << permission
		sign_in @user
		
	end
	describe "GET inde" do
		before do
			@product = FactoryGirl.create(:product)
			@product = FactoryGirl.create(:product, quantity: 0)
		end

		it "should return all products" do
			xhr :get, :index
			response.should be_success 
			parsed_json = JSON.parse(response.body)
			parsed_json["total"].should == 2
		end

		it "should return all products that can be sold" do
			xhr :get, :index, { type: "sales"}
			response.should be_success
			parsed_json = JSON.parse(response.body)
			parsed_json["total"].should == 1
		end
		

	end
end

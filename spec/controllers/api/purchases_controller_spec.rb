require 'spec_helper'

describe Api::PurchasesController do

	before do
		@user = FactoryGirl.create(:user)	
		role  = FactoryGirl.create(:role)
		permission = FactoryGirl.create(:permission, subject_class: "Purchase")
		@user.roles << role
		role.permissions << permission
		sign_in @user
	end

	describe "Get index" do
		it "returns http success" do
			xhr :get, :index
			response.should be_success
		end

		it "should have the following fields in the response" do
			xhr :get, :index
			parsed_json = JSON.parse(response.body)
			parsed_json["success"].should == true
			parsed_json["data"].should_not be_nil
			parsed_json["total"].should_not be_nil
		end
	end

	describe "Get show" do
		before do
			@purchase = FactoryGirl.create(:purchase)
		end
		it "returns a single user" do
			xhr :get, :show, :id => @purchase.id
			response.should be_success
			parsed_json = JSON.parse(response.body)
			parsed_json["success"] = true
			parsed_json["data"]["id"].should == @purchase.id
		end
	end

	describe "Post create" do
		before do
			@purchase_params = {
				:purchase => {
					:purchase_date => "2013-06-14T00:00:00.000Z", 
					:purchase_order_id => 1
				},
				:purchase_items =>[
					{ 	
						:product_id => 1, 
						:quantity => 10, 
						:supplier_id =>0, 
						:unit_cost => 5, 
						:total_cost => 50
					}
				]
			}
			@product = FactoryGirl.create(:product)
			FactoryGirl.create(:purchase_order)
		end

		context "when successful" do
			it "should create  new purhcase and its purchase items" do	
				lambda {
					lambda {
						xhr :post, :create, @purchase_params						
					}.should change(Purchase, :count).by(1)
				}.should change(PurchaseItem, :count).by(1)
			end

			it "should increase the products quantity by the new quantity in th purchase" do
				
			end

			before do
				post :create, @purchase_params
			end

			it "should be successfull" do
				response.should be_success
				parsed_json = JSON.parse(response.body)
				parsed_json["success"].should == true
				parsed_json["message"].should_not be_nil
			end
		end

		context "when failure" do
			
		end
	end
end

require 'spec_helper'

describe Api::SalesController do
	before do
		@user = FactoryGirl.create(:user)	
		role  = FactoryGirl.create(:role)
		permission = FactoryGirl.create(:permission, subject_class: "Sale")
		@user.roles << role
		role.permissions << permission
		
		sign_in @user
	end

	describe "GET index" do
		it "should return success " do
			xhr :get, :index
			response.should be_success
		end

		it "should return data in a specific format" do
			xhr :get, :index
			response.body.should_not be_nil
		end

	end

	describe "POST create" do
		before  do
			@user = FactoryGirl.create(:user)
			@sale_params = {
				:sale => {
					amount_collected: 70,
					change: 2.00,
					customer: "Selasie Hanson",
					discount: 0,
					total_amount: 68.00,	
				},
				:sale_items => [
					{
						product_id: 1,
						quantity: 2,
						selling_price: 34
					}
				]
			}

			@product = FactoryGirl.create(:product)
		end

		context "when successful" do
			it "should create a new sale and its sale item" do
				lambda { 
					lambda {
						xhr :post, :create, @sale_params
						}.should change(Sale, :count).by(1)
				}.should change(SaleItem, :count).by(1)
			end

			before do
				xhr :post, :create, @sale_params
			end

			it "should be successful" do
				response.should be_success
				parsed_json = JSON.parse(response.body)
				parsed_json["success"].should == true
				# parsed_json["message"].should_not be_nil
				

			end
		end


	end
end

require 'spec_helper'

describe RolesController do

  before do
    @user = FactoryGirl.create(:user)
    role = FactoryGirl.create(:role)
    permission = FactoryGirl.create(:permission, subject_class: "Role")
    @user.roles << role
    role.permissions << permission
    sign_in @user
  end


  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'edit'" do
    it "returns http success" do
      get 'edit'
      response.should be_success
    end
  end

end

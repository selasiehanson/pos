require 'spec_helper'

describe PurchaseOrdersController do
  before do
    @user = FactoryGirl.create(:user)
    role = FactoryGirl.create(:role, name: "users")
    permission = FactoryGirl.create(:permission, subject_class: "PurchaseOrder", action: "read")
    @user.roles << role
    role.permissions << permission
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "returns http success" do
      get 'show'
      response.should be_success
    end
  end

end

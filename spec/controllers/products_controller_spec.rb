require 'spec_helper'

describe ProductsController do

  before do
    @user = FactoryGirl.create(:user)
    role = FactoryGirl.create(:role, name: "products")
    permission = FactoryGirl.create(:permission, subject_class: "Product")
    @user.roles << role
    role.permissions << permission
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end

    it "renders the index template " do
      get :index
      response.should render_template("index")
    end
  end

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end

  

  describe "GET 'edit'" do
    it "returns http success" do
      get 'edit'
      response.should be_success
    end
  end

end

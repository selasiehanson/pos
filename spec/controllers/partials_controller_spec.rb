require 'spec_helper'

describe PartialsController do

  describe "GET 'index'" do
    it "returns http success" do
      get 'index', {model: "product_categories", partial: "_form_detail"}
      response.should be_success
    end
  end

end

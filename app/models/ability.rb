class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    
    user ||= User.new # guest user (not logged in)
    user.roles.each do |role|
      role.permissions.each do |permission|
        if permission.subject_class == "all" #might never get here
          can permission.action.to_sym, permission.subject_class.to_sym
        else
          can permission.action.to_sym, permission.subject_class.constantize
        end    
      end
    end  
  end
end

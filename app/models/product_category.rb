class ProductCategory < ActiveRecord::Base
  attr_accessible :description, :name

  validates :name, presence: true
  validates :created_by_id, presence: true
  validates :updated_by_id, presence: true

  belongs_to :created_by, class_name: "User"
  belongs_to :updated_by, class_name: "User"
  has_many :products
end

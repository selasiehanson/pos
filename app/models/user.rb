class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, 
  				:first_name, :last_name, :user_name
  # attr_accessible :title, :body

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :user_name, presence: true 

  has_many :roles_users
  has_many :roles, through: :roles_users

  has_many :created_sales, class_name: "Sale", foreign_key: :created_by_id
  has_many :updated_sales, class_name: "Sale", foreign_key: :updated_by_id
  has_many :created_products, class_name: "Product", foreign_key: :created_by_id
  has_many :updated_products, class_name: "Product", foreign_key: :updated_by_id
  has_many :created_product_categories, class_name: "ProductCategory", foreign_key: :created_by_id
  has_many :updated_product_categories, class_name: "ProductCategory", foreign_key: :updated_by_id
  

  def full_name
    "#{first_name} #{last_name}"
  end

  def as_json(option= nil)
    format = {
      except: [:created_at, :updated_at],
      :include => {:roles_users => { :include => {:role => { :only => [:id, :name] }}}} , 
      methods: [:full_name]
    }

    super(format)
  end
end

class Product < ActiveRecord::Base
  attr_accessible :description, :name, :quantity, :reorder_quantity, :selling_price, :product_category_id
  
  validates :name, presence: true
  validates :quantity, numericality: { greater_than: -1 }
  validates :reorder_quantity, numericality: { greater_than: 0 }
  validates :selling_price, numericality: { greater_than: 0.00 }
  validates :product_category_id, presence: true
  validates :created_by_id, presence: true
  validates :updated_by_id, presence: true

  belongs_to :product_category
  has_many :purchase_order_items
  has_many :purchase_items
  belongs_to :created_by, class_name: "User"
  belongs_to :updated_by, class_name: "User"

  scope :sales_items,  {
    conditions: "products.quantity > 0"
  }

  def as_json(options= {})
    format = {
      :include => :product_category
    }

    super(format)
  end
end

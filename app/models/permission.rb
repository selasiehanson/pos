class Permission < ActiveRecord::Base
  attr_accessible :action, :subject_class
  has_many :permissions_roles
  has_many :roles, through: :permissions_roles
end

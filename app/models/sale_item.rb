class SaleItem < ActiveRecord::Base
  
  attr_accessible :quantity, :selling_price, :product_id, :sale_id
  
  validates :product_id, presence: true
  # validates :sale_id, presence: true
  validates :created_by_id, presence: true
  validates :updated_by_id, presence: true
  validates :quantity, numericality: { greater_than: 0}
  validates :selling_price, numericality: { greater_than: 0}
  validates :quantity, numericality: { greater_than: 0}

  belongs_to :product
  belongs_to :sale
  belongs_to :created_by, class_name: "User"
  belongs_to :updated_by, class_name: "User"

  before_save :compute_line_total

  scope :most_popular_n_products, ->(to_take){
    {
      :joins => :product,
      :select => "SUM(sale_items.quantity) as quantity, sale_items.product_id, products.name as product_name",
      :group => "sale_items.product_id",
      :order => "quantity DESC",
      :limit => to_take
    }
  }

  def compute_line_total
  	self.line_total  = self.selling_price * self.quantity
  end
end

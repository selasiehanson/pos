class PurchaseOrder < ActiveRecord::Base
  	attr_accessible :opened, :order_date, :order_id, :purchase_order_items_attributes

  	before_save :create_order_date_and_id

  	validates :created_by_id, presence: true
  	validates :updated_by_id, presence: true

  	belongs_to :created_by, class_name: "User"
  	belongs_to :updated_by, class_name: "User"

  	has_one :purchase
  	has_many :purchase_order_items, :dependent => :destroy
    # accepts_nested_attributes_for :purchase_order_items, reject_if: :all_blank, allow_destroy: true
  	accepts_nested_attributes_for :purchase_order_items, :allow_destroy => :true,
                                  :reject_if => proc { |attrs| attrs.all? { |k, v| v.blank? } }

    scope :closed, {
      conditions: { opened: false} 
    }
    
    scope :open_orders, {
      conditions: { opened: true}
    }

    default_scope order: 'purchase_orders.created_at DESC'

  	def create_order_date_and_id  		
  		count = PurchaseOrder.where("order_date BETWEEN ? AND ?", DateTime.now.beginning_of_day, DateTime.now.end_of_day).count
  		self.order_date ||=  DateTime.now
  		self.order_id ||= "PO#{ Date.today.to_s(:number) }#{count + 1}"
  	end

    def as_json(options={})
     format = {
        :include => [
          { 
            :purchase_order_items => {
              :except => [:created_at, :updated_at],
              :include => { :product => { :only => [:name, :id]}}
            }
          },
          { :created_by => { :only => [], :methods => :full_name }}   
        ]
      }

      super(format)
    end
end

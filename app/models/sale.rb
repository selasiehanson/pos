class Sale < ActiveRecord::Base
  attr_accessible :amount_collected, :customer, :sale_number ,:total_amount, :discount, :change, :sale_items_attributes

  before_save :create_sale_number

  validates :created_by_id, presence: true
  validates :updated_by_id, presence: true
  validates :amount_collected, numericality: { greater_than: 0.0}
  validates :total_amount, numericality: { greater_than: 0.0 }
  
  #do validation for change with function
  validates :change, numericality: { greater_than_or_equal_to: 0.0 }
  validates :discount, numericality: { greater_than_or_equal_to: 0.00, less_than_or_equal_to: 100.00}

  belongs_to :created_by, class_name: "User"
  belongs_to :updated_by, class_name: "User"

  has_many :sale_items
  accepts_nested_attributes_for :sale_items, allow_destroy: true, reject_if: proc {|attrs| attrs.all? { |k, v| v.blank? } }
  # default_scope :order => "sales.created_at DESC"

  scope :total_sales_by_users, { 
    :joins => :created_by,
    :select => "sales.created_by_id, COUNT(sales.created_by_id) as sales_count, SUM(sales.total_amount) as total_amount ,users.first_name, users.last_name",
    :group => "sales.created_by_id",
    :order => "sales.total_amount DESC"
  }
  
  def as_json(options={})
    format = {
      :include => [
        {:sale_items => {:include => {:product => {only: [:name, :id]}}}},
        {:created_by => { :only => [], :methods => :full_name}} ,
        {:updated_by => { :only => [], :methods => :full_name}} 
      ]
    }
    super(format)
  end

  private
  def create_sale_number
    count = Sale.where("created_at BETWEEN ? AND ?", DateTime.now.beginning_of_day, DateTime.now.end_of_day).count
    self.sale_number ||= "SAL#{ Date.today.to_s(:number) }#{count + 1}"
  end
end

class PurchaseOrderItem < ActiveRecord::Base
  
  attr_accessible  :created_by_id, :updated_by_id, :quantity, :product_id, :purchase_order_id
  
  validates :created_by_id, :updated_by_id, :product_id, presence: true

  # validates :purchase_order_id, presence: true
  # validates :purchase_order_id, presence: true , :if => lambda {
  # 	self.purchase_order.valid?
  # }
  validates :quantity, numericality: { greater_than: 0}

  belongs_to :purchase_order
  belongs_to :product
  belongs_to :created_by, class_name: "User"
  belongs_to :updated_by, class_name: "User"
end

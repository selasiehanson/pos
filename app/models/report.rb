class Report < ActiveRecord::Base
	serialize :query_object, Hash
  attr_accessible :created_by_id, :name, :query_object, :query_string, :updated_by_id

  validates :name, presence: true, uniqueness: true

  belongs_to :created_by, class_name: "User"
  belongs_to :updated_by, class_name: "User"
end

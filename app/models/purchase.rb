class Purchase < ActiveRecord::Base
  attr_accessible :created_by_id, :purchase_by_id, :purchase_date, :purchase_order_id, :received_by_id, :updated_by_id, :purchase_items_attributes
  
  belongs_to :purchase_order

  validates :created_by_id, :updated_by_id, :purchase_by_id, :received_by_id, :purchase_order_id, presence: true

  belongs_to :created_by, class_name: "User"
  belongs_to :updated_by, class_name: "User"
  belongs_to :purchase_by, class_name: "User"
  belongs_to :received_by, class_name: "User"

  has_many :purchase_items, :dependent => :destroy

  accepts_nested_attributes_for :purchase_items, allow_destroy: true, reject_if: proc {|attrs| attrs.all? { |k, v| v.blank? } }

  before_save :add_purchase_date_and_purchase_id

  def add_purchase_date_and_purchase_id
  	count = Purchase.where("purchase_date BETWEEN ? AND ?", DateTime.now.beginning_of_day, DateTime.now.end_of_day).count
  	self.purchase_date ||= DateTime.now
  	self.purchase_id ||= "PU#{ Date.today.to_s(:number) }#{count + 1}"
  end

  def as_json(options={})
    format = {
      # :except => [:created_at, :updated_at],
      :include =>  [
          {:purchase_items => { :except => [:created_at, :updated_at], :include => {:product => {:only => [:name, :id] }} }}, 
          {:purchase_by => { :only => [], :methods => :full_name}} ,
          {:received_by => { :only => [], :methods => :full_name}} 
        ]
    }

    super(format)
  end
end

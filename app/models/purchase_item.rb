class PurchaseItem < ActiveRecord::Base
  attr_accessible :created_by_id, :product_id, :purchase, :quantity, :supplier_id, :total_cost, :unit_cost, :updated_by_id

  validates :created_by_id, presence: true
  validates :updated_by_id, presence: true
  # validates :purchase_id, presence: true
  validates :product_id, presence: true
  validates :quantity, numericality: { greater_than: 0}
  validates :total_cost, numericality: {greater_than: 0.0}
  validates :unit_cost, numericality: { greater_than: 0.0 }

  belongs_to :product
  belongs_to :purchase
  belongs_to :created_by, class_name: "User"
  belongs_to :updated_by, class_name: "User"
end

class UsersController < BaseController
	load_and_authorize_resource
	before_filter :load_permissions
  def index
  end

  def show
  end

  def edit
  	render "edit"
  end
end

class BaseController < ApplicationController 
	layout false
	
	rescue_from CanCan::AccessDenied do	
		file  = File.open("#{Rails.root}/public/403.html")
    render :json => file.read
	end
end

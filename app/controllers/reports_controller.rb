class ReportsController < BaseController
  before_filter :create_design_handler
  before_filter :fetch_report, only: [:show, :update]

  def index
    reports = Report.all
    render :json => { data: reports, message: "", success: true, total: reports.length}
  end

  def designer
    
  end

  def models
  	data = @dh.app_models
		render :json => 	{data: data, total: data.length}
  end

  def run
    result = @dh.run_query(params)
    render :json => { 
      :in => params, 
      :query_fields => result[:query_fields] , 
      :sql => result[:sql],
      :data => result[:data]
    }
  end

  def show
     render :json => {  data: @report, success: success, message: ""}
  end

  def create
    success = false
    report = Report.new(params[:report])
    report.created_by = report.updated_by = current_user
    if report.save
      success = true
      render :json => {  data: report, success: success, message: "Report created successfully"}
    else
      err_msg = report.errors.full_messages.to_sentence
      render :json => { data: nil, message: err_msg, success: success}
    end
  end

  def update
    success = false
    @report.update_attributes(params[:report])
    if @report.save
      success = true
      render :json => { data: @report, success: success, message: "Report updated successfully"}
    else
      err_msg = @report.errors.full_messages.to_sentence
      render :json => { data: nil, message: err_msg, success: success}
    end
  end

  private 
    def create_design_handler
      @dh = DesignHandler::Push.new
    end

    def fetch_report
      @report = Report.find(params[:id])
    end

end

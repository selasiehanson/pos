class Api::ProductCategoriesController < ApplicationController
  load_and_authorize_resource
  def index
    data = ProductCategory.all
  	render :json => { :data => data, total: data.size , success: :success}
  end

  def show
  	product_category = ProductCategory.find(params[:id])
  	render :json => product_category
  end

  def create
  	product_category = ProductCategory.new(params[:product_category])
  	product_category.created_by = product_category.updated_by = current_user
  	if product_category.save
      msg = "Product Category saved successfully"
  		render :json => { :data => product_category, message: msg,success: :success}
  	else
  		err_msg = product_category.errors.full_messages.to_sentence
  		render :json => { data: nil, message: err_msg , success: :error}
  	end
  end

  def update
  	product_category = ProductCategory.find(params[:id])
  	if product_category
  		product_category.update_attributes(params[:product_category])
  		if product_category
        msg = "Product Category updated successfully"
  			render :json => { data: product_category , message: msg, success: :success}
  		else
  			err_msg = product_category.errors.full_messages.to_sentence
  			render :json => { data: nil, message: err_msg, success: :error }
  		end
  	else
  		render :json => { success: :error, data: { message: "No product category with the specified id was found"}}
  	end
  end

  def destroy

  end
end

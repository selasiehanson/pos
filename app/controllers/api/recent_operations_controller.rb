class Api::RecentOperationsController < Api::BaseController

	# skip_authorization_check
	# authorize_resource :class => false
	def index
		out = []
		
		last = Sale.order("updated_at DESC").first
		out << output_for_recent(Sale.model_name.human,"sales/#{last.id}/", last.sale_number , "show")
		
		last = Purchase.order("updated_at DESC").first
		out << output_for_recent(Purchase.model_name.human, "purchases/#{last.id}/", last.purchase_id, "show")

		last = PurchaseOrder.order("updated_at DESC").first
		out << output_for_recent(PurchaseOrder.model_name.human, "purchase_orders/#{last.id}/",last.order_id)
		
		last = Product.order("updated_at DESC").first
		out << output_for_recent(Product.model_name.human,"products/#{last.id}/", last.name)

		last = ProductCategory.order("updated_at DESC").first
		out << output_for_recent(ProductCategory.model_name.human, "product_categories/#{last.id}/", last.name)
    render :json => { data: out, total: out.length, success: true}
	end

	private
	def output_for_recent(type,path ,name,method = "edit")
		
		{ 
			type: type,
      url: "##{path}#{method}",
      name: name
		}
	end
end

class Api::ProductsController < Api::BaseController
	load_and_authorize_resource
  before_filter :load_permissions
	def index
		case params[:type]
			when "sales"
				data = Product.sales_items
				total = Product.sales_items.count
			else
				data = Product.all
				total = Product.count
			end
		render :json => { data: data, total: total }
	end
	
	def show
		product = Product.find(params[:id])
		respond_with :api, product
	end

	def create
		product = Product.new(params[:product])
		product.created_by = product.updated_by = current_user
		
		# do validations later
		if product.save
			msg = "Product saved successfully"
			render :json => {:data => product, message: msg, :success => :success}
		else
			err_msg = product.errors.full_messages.to_sentence
			render :json => { data: nil,  message: err_msg , success: :error}
		end
	end

	def update	
		product.update_attributes(params[:product])
		if product.save
			msg = "Product updated successfully"
			render :json => { data: product, message: msg, success: :success}
		else
			err_msg = product.errors.full_messages.to_sentence
			render :json => { data: nil,  message: err_msg, success: :error}
		end
	end

	def destroy
		
	end
end

class Api::SalesController < Api::BaseController
	load_and_authorize_resource
	def index
		sales = Sale.all(order: "sales.created_at DESC")
		render json: { data: sales, total: sales.size, success: true}
	end

	def show
		sale = Sale.find(params[:id])
		if sale
			render json: { data: sale, success: true }
		else 
			render json: { data: nil, success: false }	
		end
		
	end

	def create
		params[:sale][:sale_items_attributes] = params[:sale_items]
		sale = Sale.new(params[:sale])
		sale.created_by = sale.updated_by = current_user

		sale.sale_items.each do |item|
			item.created_by = item.updated_by = current_user
		end

		if sale.save
			render json: {data: sale, success: true, message: "Sale saved successfully"}
		else
			err_msg = sale.errors.full_messages.to_sentence
			render json: {data: nil,message: err_msg, success: false}
		end
	end

	def update
		
	end

	def destroy
		
	end
end

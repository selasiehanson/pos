class Api::DashboardController < ApplicationController
	# load_and_authorize_resource
	def sales_by_users
		result = Sale.total_sales_by_users
		render :json => { data: result, total: result.length, success: true }
	end

	def most_popular_products
		n = 5 #change this to support a query parameter of later which defaults to 5 when none is provided
		result =  SaleItem.most_popular_n_products(n)
		render :json => { data: result, total: result.length, success: true}
	end
end

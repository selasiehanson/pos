class Api::PermissionsController < Api::BaseController

	def index
		render :json => { data: Permission.all, total: Permission.count, success: true}
	end
end

class Api::PurchaseOrdersController < Api::BaseController
	load_and_authorize_resource
	def index
		case params[:status]
			when "open"
				data = PurchaseOrder.open_orders
				total = PurchaseOrder.open_orders.count
			when "closed"
				data = PurchaseOrder.closed
				total = PurchaseOrder.closed.count
			else
				data = PurchaseOrder.all
				total = PurchaseOrder.count
			end
				
		out = { 
			data: data, 
			success: true,
			total: total
		}
		render :json => out
	end

	def show
		# todo check where order does not exist
		render :json => { data: PurchaseOrder.find(params[:id]),  success: true}
	end

	def create
		params[:purchase_order][:purchase_order_items_attributes] = params[:purchase_order_items]
		purchase_order = PurchaseOrder.new(params[:purchase_order])
		purchase_order.created_by = purchase_order.updated_by = current_user

		# we need to update the created by and updated by columns of the items
		purchase_order.purchase_order_items.each do | item|
			item.created_by = item.updated_by = current_user
		end

		if purchase_order.save
			msg = "Purchase Order saved successfully."
			render json: { data: purchase_order, message: msg, success: true}
		else
			err_msg = purchase_order.errors.full_messages.to_sentence
			render json: { data: nil, message: err_msg , success: false}
		end
	end

	def update
		
	end

	def destroy
		purchase_order  = PurchaseOrder.find(params[:id])
		if purchase_order.destroy
			render :json => {message: "Purchase Order deleted successfully.", success: true}
		else
			render :json => { message: purchase_order.errors.full_messages.to_sentence, success: false}
		end
	end
end

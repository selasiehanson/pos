class Api::BaseController < ApplicationController
	layout false
	respond_to :json
	before_filter :authenticate_user!	

	rescue_from CanCan::AccessDenied do
		msg =  "Sorry, your access does not allow you to perform the requested operation"
		render :json => { data: nil, message: msg, success: false}
	end
end

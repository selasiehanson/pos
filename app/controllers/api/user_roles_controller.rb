class Api::UserRolesController < ApplicationController
	def index
		
	end

	def create
		user = User.find(params[:user_id])
		additions = params[:additions] || []
		removals = params[:removals] || []
		status =  false
		# TODO check for validity of user first
		begin
			ActiveRecord::Base.transaction do
				removals.each do |role_id|
					RolesUser.destroy_all(:user_id => user.id, :role_id => role_id)
				end

				additions.each do |role_id|
					user_role = RolesUser.where(:user_id => user.id, :role_id => role_id).first_or_create
				end	
			end
			status = true
		rescue Exception => e
			p e
		end

		if status
			render :json => {success: true, message: "Successfully updated roles for user #{user.full_name}."}
		else
			render :json => { success: false , message: "There was a problem saving the records. Please try again."}
		end
		
	end
end

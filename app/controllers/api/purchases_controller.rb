class Api::PurchasesController < Api::BaseController
	load_and_authorize_resource
	def index
		data = Purchase.all
		out = {
			data: data,
			success: true, 
			total: data.size
		}

		render :json => out
	end

	def show
		render :json => { data: Purchase.find(params[:id]), success: true}
	end

	def create
		params[:purchase][:purchase_items_attributes] = params[:purchase_items]
		purchase = Purchase.new(params[:purchase])
		purchase.created_by = purchase.updated_by = purchase.purchase_by = purchase.received_by = current_user
		
		purchase.purchase_items.each do |item|
			item.created_by = item.updated_by = current_user
		end
		
		success = false
		purchase.transaction do
			success = purchase.save
			# check to make the order exists
			purchase.purchase_order.opened = false
			purchase.purchase_order.save

			purchase.purchase_items.each do |item|
				item.product.increment(:quantity, item.quantity)
				item.product.save
			end
		end

		if success
			render :json => {data: purchase, message: "Purchase created successfully",success: true }
		else
			err_msg = purchase.errors.full_messages.to_sentence
			render :json => { data: nil, message: err_msg, success: false}
		end
	end

	def update
		
	end

	def destroy
		
	end
end

class Api::PermissionsRolesController < Api::BaseController
  # load_and_authorize_resource
  def index
  	role_id = params[:role_id]
  	render :json => { data: PermissionsRole.where(role_id: role_id), success: true, message: ""}
  end

  def show
  end

  def create
  	role = Role.find(params[:role_id])
  	additions = params[:additions] || []
  	removals = params[:removals] || []
  	status =  false

  	begin
  		ActiveRecord::Base.transaction do
  			removals.each do |permission_id|
  				PermissionsRole.destroy_all(:permission_id => permission_id, :role_id => role.id)
  			end

  			additions.each	do |permission_id|
  				PermissionsRole.where(:permission_id => permission_id, :role_id => role.id).first_or_create
  			end
  			status = true
  		end
  	rescue Exception => e
  		
  	end

  	if status
  		render :json => { success: true, message: "Permissions updated successfully for role #{role.name}"}
  	else
  		render :json => { success: false , message: "There was a problem saving the records. Please try again."}
  	end
  end
end

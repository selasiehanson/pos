class Api::UsersController < Api::BaseController
	load_and_authorize_resource
	def index
		users = User.all(order: "created_at DESC")
		render :json => { data: users, total: User.count, status: true,}
	end

	def show
		user = User.find(params[:id])
		if user
			render :json => { data: user, success: true, message: ""}
		else
			render :json => { data: nil, success: false, message: "User was not found"}
		end
	end

	def update
		
	end

	def delete
		
	end
end
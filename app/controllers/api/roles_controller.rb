class Api::RolesController < Api::BaseController

  def index
    render :json => { data: Role.all, total: Role.count, success: :success}
  end

  def show
    role = Role.find(params[:id])
    if role
      render :json => { data: role, success: true}
    else
      render :json => { data: nil, success: false, message: "Role was not found"}
    end
  end

  def create
    role = Role.new(params[:role])
    # role.created_by = role.updated_by = current_user
    if role.save
      msg = "Role saved successfully"
      render :json => { :data => role, message: msg,success: :success}
    else
      err_msg = role.errors.full_messages.to_sentence
      render :json => { data: nil, message: err_msg , success: :error}
    end
  end

  def update
    role = Role.find(params[:id])
    if role
      role.update_attributes(params[:role])
      if role
        msg = "Role updated successfully"
        render :json => { data: role , message: msg, success: :success}
      else
        err_msg = role.errors.full_messages.to_sentence
        render :json => { data: nil, message: err_msg, success: :error }
      end
    else
      render :json => { success: :error, data: nil, message: "No product category with the specified id was found"}
    end
  end

  def delete
  end
end

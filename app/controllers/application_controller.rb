class ApplicationController < ActionController::Base
  protect_from_forgery

  def index
  	if user_signed_in?
  		render layout: "main", nothing: true
  	else
  		redirect_to new_user_session_path
  	end
  end

  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found


  protected

    #derive the model name from the controller. eg UsersController will return User
    def self.permission
      return name = self.name.gsub('Controller','').singularize.split('::').last.constantize.name rescue nil
    end
   
    def current_ability
      @current_ability ||= Ability.new(current_user)
    end
   
    #load the permissions for the current user so that UI can be manipulated
    def load_permissions
      # @current_permissions = current_user.role.permissions.collect{|i| [i.subject_class, i.action]}
      @current_permissions =  []
      current_user.roles.each do |role|
      	@current_permissions << role.permissions.collect{|i| [i.subject_class, i.action]}
      end
    end

    def record_not_found
      
    end
end

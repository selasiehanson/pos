
'use-strict';

var app = angular.module('app');

app.controller("PurchaseOrdersController", function ($scope, $rootScope, PurchaseOrder,MSG){
	$scope.purchaseOrders = [];

    PurchaseOrder.query(function (res){
        $scope.purchaseOrders = res.data
    });

    //checks to see whether this order has been closed by a purchase
    $scope.isClosed = function (status){
        if(status){
            return "badge-success";
        }
        return "badge-important";
    }

    $scope.destroy = function (_purchaseOrder, index){
        var purchaseOrder =  new PurchaseOrder(_purchaseOrder);
        if(!purchaseOrder.opened){
            MSG.error("Can't delete this order. A purchase has already been made against it");
            return;
        }
        purchaseOrder.$delete(function (res){
            if(res.success){
                $scope.purchaseOrders.splice(index, 1);
                MSG.success(res.message)
            }else {
                MSG.error(res.message)    
            }
            
        });
    }
});

app.controller("PurchaseOrdersShowController", function ($scope, $rootScope, $routeParams, PurchaseOrder , Printer){
    PurchaseOrder.get({id: $routeParams.id}, function (res){
        $scope.purchase_order = res.data
    });

    $scope.printSale = function (id){
        Printer.print({ model:"purchase_orders", id : id})
    }

});

app.controller("PurchaseOrdersNewEditController", function ($scope, $rootScope, $location, Product, PurchaseOrder, MSG){

	Product.query(function (res){
		$scope.products = res.data;
	});

	$scope.orderItems = [];	

	$scope.addProductToList = function (newOrderItem){

        if(newOrderItem.quantity < 0 || !newOrderItem.quantity){
            newOrderItem.quantity = 1;
        }

        var product = $scope.newOrderItem.product;
        var obj = {
            quantity: newOrderItem.quantity,
            product: product.name,
            product_id: product.id
        };
        var items = findItemInOrder(obj);
        if(items.length === 0){
            $scope.orderItems.push(obj);
        }else {
            items[0].quantity += newOrderItem.quantity;
        }
    }

    $scope.lessenProdcut = function($index){
        var item = $scope.orderItems[$index];
        if(item.quantity > 1)
            item.quantity--;
    }

    $scope.removeProduct = function(index) {
        var item = $scope.orderItems.splice(index, 1);
        if($scope.editMode){
            $scope.deletedItems.push(item[0].id);
        }
    }

    function findItemInOrder (obj){
            
        var result = $scope.orderItems.filter(function (n){
            return n.product_id === obj.product_id;
        });
        return result;
    }

	$scope.save =  function (){
		// var thePurchaseOrder = OBJ.rectify(angular.copy(newPurchaseOrder), _default);
        var thePurchaseOrder = angular.copy($scope.newPurchaseOrder);

        if(!thePurchaseOrder["order_date"]){
            MSG.show("Please select a date. The purchase order must have a date");
            return;
        }

        thePurchaseOrder["purchase_order_items"] = $scope.orderItems ;
        if(thePurchaseOrder["purchase_order_items"].length === 0){
            MSG.show("Please add at least one item to the purchase order");
            return;            
        }

        if (thePurchaseOrder["id"]) { //this is an update
            thePurchaseOrder["deletedItems"] = $scope.deletedItems;
            PurchaseOrder.update(thePurchaseOrder, function(res) {
                afterSave(res);
            });
        } else {
            //we are creating a new purchase order
            var _purchaseOrder =  angular.copy(thePurchaseOrder);
            var items = _purchaseOrder.purchase_order_items.map(function (item){
              delete item.product;
              return item;
            });

            _purchaseOrder.purchase_order_items = items;
            
            var purchaseOrder = new PurchaseOrder(_purchaseOrder);
            purchaseOrder.$save(function (res) {
                afterSave(res);
            });
        }	
	}

	function afterSave(res){
        if(res.success){
            $location.path("purchase_orders");
            MSG.success(res.message)
        }else {
            MSG.error(res.message)
        }

	}
});

'use strict';

var app = angular.module('app');

app.controller("ProductCategoriesController", function ($scope, $rootScope, ProductCategory){
	ProductCategory.query(function (res){
		$scope.productCategories = res.data;
	});
});


// TODO create generic lookup service
app.controller("ProductCategoriesNewEditController", function ($scope, ProductCategory, $routeParams,$location, MSG){

	if($routeParams.id){
		ProductCategory.get({id: $routeParams.id},function (res){
			$scope.lookup = res;
		});
	}

	$scope.save = function  () {
		var productCategory =  new ProductCategory($scope.lookup);
		if(productCategory.id){
			productCategory.$update(function (res){
				afterSave(res)
			});
		}else {
			productCategory.$save(function (res){
				afterSave(res)
			});	
		}

		function afterSave(res){
			if(res.status){
				$location.path("product_categories");
				MSG.success(res.message)
			}	
		}
		
	}
});
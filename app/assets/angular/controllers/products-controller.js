'use-strict';

var app = angular.module('app');

app.controller("ProductsController", function (Product,$scope, $rootScope){
	$scope.products = [];
	Product.query(function (res){
		$scope.products = res.data;
	});

	$scope.destroy = function (){
		alert("removing")
	}
});

app.controller("ProductsNewEditController", function ($scope, Product, ProductCategory, $routeParams, $location){

	$scope.getProductCategories =  function (){
		ProductCategory.query(function (res){
			$scope.productCategories = res.data;
		});
	}
	if($routeParams.id){
		Product.get({id: $routeParams.id}, function (res){
			$scope.newProduct = res
		});
	}

	$scope.save =  function (){
		var product  = new Product($scope.newProduct);

		if(product.id){
			//updating
			product.$update(function (res){
				afterSave(res)
			})
		}else {
			product.$save(function (res){
				afterSave(res);
			});	
		}

		function afterSave(res){
			if(res.status){
				$location.path("products");	
			}
		}
	}

	$scope.getProductCategories();

});

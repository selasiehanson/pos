'use-strict';

var app = angular.module('app');
app.service("Dashboard", function ($http){
	return {
		//change the callback to use promises later
		get : function (path,callback){
			$http.get("api/" + path).then(function (res){
				callback(res.data.data);
			});
		
		}
	}
});

app.controller("HomeController", function ($scope, $rootScope, Dashboard){
	
	$scope.getColumnData = function(){
		Dashboard.get("sales_by_users", function (data){
				var amountData = _.map(data,function (x){ 
					return {
						name: x.first_name + " "+ x.last_name,	
						data: [ parseFloat(x.total_amount)]
					}
				});

				var countData = _.map(data,function (x){ 
					return {
						name: x.first_name + " "+ x.last_name,	
						data: [ parseFloat(x.sales_count)]
					}
				});
				var amountConfig = $scope.totalAmount = {
					id: "salesAmountChart",
					title : "Total Amount of sales per Sales Person",
					categories : ["Total Amount"],
					series: amountData
				};



				var countConfig = $scope.salesCount = {
					id: "salesCountChart",
					title : "Number of items sold per Sales Person",
					categories : ["Sales Count"],
					series: countData
				};
			// var child =  $rootScope.$new();
			// child.$emit("chartDataReceived", amountConfig);
			// child.$emit("chartDataReceived", countConfig);
			
		});
	}

	function getMostPopularProducts(){
		Dashboard.get("most_popular_products", function (res){
			$scope.mostPopularProducts = res;
		});
	}
	
	function start(){
		$scope.getColumnData();
		getMostPopularProducts();	
	}

	start();
});

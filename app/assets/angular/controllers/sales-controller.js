'use-strict';

var app = angular.module('app');

app.controller("SalesController", function ($scope, $rootScope, Sale, Printer){
	$scope.sales = [];
	Sale.query(function (res){
		$scope.sales = res.data
	});

	$scope.print = function (id){
		Printer.print({ model:"sales", id : id})
	}
});

app.controller("SalesShowController", function ($scope, $rootScope, $routeParams, Sale , Printer){
	Sale.get({id: $routeParams.id}, function (res){
		$scope.sale = res.data
	});

	$scope.print = function (id){
		Printer.print({ model:"sales", id : id})
	}

});

app.controller("SalesNewController", function ($scope, $rootScope, $location, $filter, Product, MSG , Sale){
	
	$scope.itemsToSell = [];
	
	$scope.selectedItem = {};

	$scope.itemsPurchased = [];

	$scope.newSale = { total_amount : 0 , discount: 0};

	$scope.todaysSales = [];

	var currencyFilter = $filter("currency");

	function getSalesProducts(){
		Product.query({type: "sales"},function (res){
			$scope.itemsToSell = res.data
		});
	}

	$scope.create = function (){
		
	}

	$scope.showMessage = function (){
		MSG.show("some message", "success");
	}

	getSalesProducts();

	$scope.addToSale = function(){
		
		//var obj = $scope.selectedItem;
		var item = angular.copy($scope.selectedItem);
		if(item){
			item = item.product;
			// console.log(item)
			var items = findItemInPurchased(item);
			if(items.length === 0){
				var obj = {
					name: item.name,
					product_id: item.id,
					quantity_remaining : item["quantity"],
					quantity : 1,
					selling_price: item.selling_price,
					line_total : 0
				} 
				$scope.itemsPurchased.push(obj);
			}
			else {
				if (items[0].quantity_left > 0){
					items[0].quantity++;	
				}else {
					MSG.show("Can't add more of " + items[0].name, "info");
				}
			}
		}else {
			MSG.show("No item selected")
		}
		
		$scope.itemsChanged();
	}

		function findItemInPurchased (item){
		
		var result = $scope.itemsPurchased.filter(function (n){
			return n.product_id === item.id;

		});
		return result;
	}

	$scope.itemsChanged = function(){
		$scope.newSale.total_amount = 0;
		$scope.itemsPurchased.forEach(function (n){
			$scope.newSale.total_amount = parseFloat($scope.newSale.total_amount) + (parseFloat(n.selling_price) * parseInt( n.quantity));
			$scope.newSale.total_amount = currencyFilter($scope.newSale.total_amount, "");
		});		
	};

	$scope.$watch("newSale.total_amount", calculateChange);
	$scope.$watch("newSale.amount_collected", calculateChange);
	
	function calculateChange (){
		$scope.newSale.change = currencyFilter($scope.newSale.amount_collected - $scope.newSale.total_amount,"");
	}
	$scope.removeItemFromSale = function(index){
		$scope.itemsPurchased.splice(index,1);
		$scope.itemsChanged();
	}

	$scope.makeSale = function (){
		
		if($scope.itemsPurchased.length === 0){
			MSG.show("Cannot make with no Items. Please add at least one item");
			return;
		}

		// var aSale = OBJ.rectify(angular.copy($scope.newSale), _default);
		var aSale = angular.copy($scope.newSale);

		aSale["sale_items"] = $scope.itemsPurchased.map(function (item){
			return {
				product_id: item.product_id,
				quantity : item.quantity,
				selling_price : item.selling_price
			}
		})
		var sale = new Sale(aSale);
		sale.$save(function (res){
			var msg = "";
			msg = res.message;
			if(res.success){
				var id = res.data.id;
				MSG.show(msg,"success",true);
				
				if($scope.printReceipt){
					$scope.printSale(id, true);
				}
			$location.path('sales');
				
			}else{
				MSG.show(msg)	
			}
		});
	}





});
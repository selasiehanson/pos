'use-strict';

var app = angular.module('app');

app.controller("PurchasesController", function ($scope, $filter, Purchase, Aggregate, Printer){
	Purchase.query(function (res){
		$scope.purchases = res.data;
	});

	$scope.totalCost = function (items){
		return $filter('currency')(Aggregate.SUM(items, "total_cost"));
	};

		$scope.print = function (id){
			Printer.print({ model:"purchases", id : id})
		}
});

app.controller("PurchasesShowController", function ($scope, $filter ,$rootScope, $routeParams, Purchase , Printer, Aggregate){
	Purchase.get({id: $routeParams.id}, function (res){
		$scope.purchase = res.data
	});

	$scope.print = function (id){
		Printer.print({ model:"purchases", id : id})
	}

	$scope.totalCost = function (items){
		if (items){
			return $filter('currency')(Aggregate.SUM(items, "total_cost"));	
		}
	};

});

app.controller("PurchasesNewController", function ($scope, $location, PurchaseOrder, Purchase){
	$scope.purchaseOrders = [];

	PurchaseOrder.query({status: "open"},function (res){
		$scope.purchaseOrders = res.data;
	})

	$scope.loadOrderItems = function (){
		
		var items = $scope.dataIn.purchaseOrder.purchase_order_items;
		
		var dataForGrid = items.map(function (item){
			return {
				product: item.product.name,
				product_id: item.product_id,
				quantity: 0,
				unit_cost: 0,
				total_cost: 0,
				supplier_id: 0 
			}
		});

		$scope.purchaseItems = dataForGrid;
	}

	$scope.save =  function (){
		var items = angular.copy($scope.purchaseItems);
		items.forEach(function (item){
			delete item.product;
		});
		var dataOut = {
			purchase_date : $scope.newPurchase.purhcase_date,
			purchase_order_id : $scope.dataIn.purchaseOrder.id,
			purchase_items: items
		}
		
		var purchase = new Purchase(dataOut);
		purchase.$save(function (res){
			afterSave(res);
		});

		function afterSave(res){
			if(res.success){
				$location.path("purchases");
			}
		}
	}
});
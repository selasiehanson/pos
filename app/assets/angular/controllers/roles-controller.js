'use strict';
'use strict';
var app = angular.module('app');

app.controller("RolesController", function ($scope, $rootScope, Role){
	Role.query(function (res){
		$scope.roles = res.data;
	});

});

app.controller("RolesNewEditController", function ($scope, Role, $routeParams,$location, MSG){

	if($routeParams.id){
		Role.get({id: $routeParams.id},function (res){
			$scope.newRole = res.data;
		});
	}

	$scope.save = function  () {
		var role =  new Role($scope.newRole);
		if(role.id){
			role.$update(function (res){
				afterSave(res)
			});
		}else {
			role.$save(function (res){
				afterSave(res)
			});	
		}

		function afterSave(res){
			if(res.status){
				$location.path("roles");
				MSG.success(res.message)
			}	
		}
		
	}
});
'user strict';

var app = angular.module("app");
app.controller("PermissionsController", function ($scope, Permission, Role, RolePermission, MSG){
	// variables to hold additions and removal of permissions
	var additions = [];
	var removals = [];

	Role.query(function (res){
		$scope.roles = res.data;
	});

	function getPermissions (){
		Permission.query(function (res){
			$scope.permissions = res.data;
			var grouped = _.groupBy($scope.permissions, "subject_class");
			var models = _.keys(grouped)
			var actions = _.values(grouped)
			$scope.groupedPermissions = [];
			for (var i = 0; i < models.length; i++) {
				$scope.groupedPermissions.push({name : models[i], actions : actions[i], checked : false}) 
			};
		});	
	}

	function sortOutPermissionsForRole(){
		for (var i = 0; i < $scope.groupedPermissions.length; i++) {
			var actions = $scope.groupedPermissions[i]["actions"];
			for (var j = 0; j < actions.length; j++) {
				actions[j]["checked"] = false;
				for (var k = 0; k < $scope.currentRolePermissisions.length; k++) {
					if (actions[j]["id"]  === $scope.currentRolePermissisions[k]["permission_id"]) {
						additions.push(actions[j]["id"]);
			 			actions[j]["checked"] = true; 
			 		}
			 	}
			}
		}
	}
	

	$scope.$watch("currentRole", function (val){
		//get the current roles permission and set it
		if (val) {
			RolePermission.get({role_id: val}, function (res){
				$scope.currentRolePermissisions = res.data;
				// clear the addition and removals
				additions = [];
				removals = [];
				sortOutPermissionsForRole();
			});
		}
	});

	$scope.toggleChecked = function (action){
		var idx = null;
		if(action.checked){
			//first remove from removals
			idx = _.indexOf(removals,action.id)
			if (idx > -1) {
				removals.splice(idx, 1);
			}; 
			additions.push(action.id)
		}else {
			idx = _.indexOf(additions, action.id)
			console.log(idx)
			if (idx > -1) {
				additions.splice(idx, 1);
			}; 
			removals.push(action.id)
		}
		
	}

	$scope.save = function (){
		console.log(additions)
		console.log(removals);
		if ($scope.currentRole) {
			var data = {
				additions : additions,
				removals : removals,
				role_id: $scope.currentRole
			}

			var rolePermission = new RolePermission(data);
			rolePermission.$save(function (res){
				if(res.success){
					MSG.success(res.message);
				}else {
					MSG.error(res.message);
				}
			});
		}else {
			MSG.error("Please select a role and make changes")
		}
		
	}

	getPermissions();
});
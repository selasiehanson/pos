
'use-strict';
function ReportsDesignerController ($scope, $rootScope,Report, MSG) {
	window.sc = $scope;
	var reportDefinition = {
		name : "",
		query_object : {}
	}

	$scope.reportToOpen = {}
	window.ff = $scope.reportFields = [];
	window.gg = $scope.groupFields = [];
	window.fl = $scope.filterFields = [];
	window.od = $scope.orderFields  = [];

	$scope.query = null;
	$scope.showQuery = false;
	$scope.showOutput = false;
	$scope.showProps = true;
	window.as = $scope.reportModelChains = [];
	$scope.testData = {
		headers: [],
		results: []
	}
	
	$scope.reports = [];	
	$scope.reportModels = [];
	$scope.runningQuery = false;

	$scope.cancelDialog = function () {
		$scope.show_create_report = false;
		$scope.show_open_report = false;
	}

	$scope.filterTypes = ["LIKE",">", ">=", "<", "<=", "=", "!="]

	var aggFunctions = {
		numbers : ["COUNT", "SUM", "AVG" ,"MIN", "MAX", "None"],
		dates : ['DATE', "None"]
	}
	
	function getModels () {
		Report.models().success(function (res){
			window.models = $scope.models = res.data.map(function(model){
				var out = model.fields.map(function (field){
					return {
						name: field.name,
						type: field.type,
						aggregate: null,
						checked: false
					}
				});

				model.fields = out;
				return model
			});
		});
	}

	$scope.appropriateFunctions  =  function (type){
		var out = [];
		switch(type){
			case "integer":
			case "float":
			case "decimal":
				out = aggFunctions.numbers;
				break;

			case "datetime":
			case "date":
				out  = aggFunctions.dates
				break;

			case "string":
			default:
				out = []
				break;
		}
		return out;
	}

	$scope.showFields =  function (model){
		_showFields(model);
	}

	function _showFields (model){
		$scope.selectedModel = model;
		if ($scope.reportModelChains.length === 0) {
			pushIntoChain(model, null);
		}
	}

	function pushIntoChain(model,relatedAssociation){
		model = model || {};
		model["_related_association"] = relatedAssociation;
		$scope.reportModelChains.push(model);
	}

	$scope.$watch("reportFields",function (val){
		if (val) {
			// console.log(val)
		}	
	},true);

	$scope.toggleInReportFields = function(field){
		window.selM = $scope.selectedModel;
		if (field.checked) {
			//add to list
			var modelName = $scope.selectedModel.name;
			
			var input = {
				field : field.name,
				type: field.type,
				model: $scope.selectedModel.name,
				table_name: $scope.selectedModel.table_name,
				alias: "",
				key: "",
				display: true
			}

			$scope.reportFields.push(input);
		}else {
			//find field in list
			var item = _.find($scope.reportFields, function (item){
			  return item.field === field.name
			});

			//remove found field from the report fields
			$scope.reportFields.splice($scope.reportFields.indexOf(item), 1);

			//TODO: remove item from query select and models and joins and conditions
		}

		$scope.query = window.q = generateQueryStructure();
	}

	/**
	 * Add or removes a field from the possible set of group fields
	 * @param  {object} field Field to add or remove
	 * @return {void}      
	 */
	$scope.toggleInGroups = function (field){
		field = angular.copy(field);
		var found  = _.find($scope.groupFields, function (item){
			return field.field === item.field;
		});

		if (found) {
			var idx = $scope.groupFields.indexOf(found)
			$scope.groupFields.splice(idx, 1);
		}else {
			angular.extend(field, {checked: false})
			$scope.groupFields.push(field);
		}
	}

	/**
	 * Adds or removes a field from the possible set of filter
	 * @param  {object} field Field to add or remove 
	 * @return {void}      
	 */		
	$scope.toggleInWheres =  function (field){
		field = angular.copy(field);
		var found  = _.find($scope.filterFields, function (item){
			return field.field === item.field;
		});

		if (found) {
			var idx = $scope.filterFields.indexOf(found)
			$scope.filterFields.splice(idx, 1);
		}else {
			var obj = {
				checked: false,
				filterType : null,
				value : null
			}	
			angular.extend(field, obj);
			$scope.filterFields.push(field);
		}
	}

	/**
	 * Add or removes a field from the possible set of orders
	 * @param  {Object} field The field to add or remove
	 * @return {void}       
	 */
	$scope.toggleInOrders = function (field){
		field = angular.copy(field);
		var found  = _.find($scope.orderFields, function (item){
			return field.field === item.field;
		});

		if (found) {
			var idx = $scope.orderFields.indexOf(found)
			$scope.orderFields.splice(idx, 1);
		}else {
			var obj = {
				checked: false
			}	
			angular.extend(field, obj);
			$scope.orderFields.push(field);
		}
	}

	/**
	 * Toggles whether a field should be part of the select query
	 * @param  {Object} item The field to be hidden or included
	 * @return {void}      
	 */
	$scope.toggleFieldInOutputView = function (item){
		!item.display;
		$scope.query = generateQueryStructure();
	}

	$scope.aggregateChanged =  function (){
		$scope.query = generateQueryStructure();
	}

	/**
	 * Generate a structure that can be sent to server side 
	 * this should be called everytime a field is added or removed 
	 * or anytime a field is hidden from the view or an aggregate 
	 * function such as count or max is applied.
	 * @return {object}
	 */
	function generateQueryStructure (){
		var _query = getDefaultQuery();
		$scope.reportFields.forEach(function (field){
			//retrieve uppercase characters in model name
			//so UserType returns UT
			var modelKey = field.model.replace(/[a-z]/g,'').toLowerCase();

			var modelKeys = _.keys(_query.models);
			var modelValues = _.values(_query.models);
			
			//this model is already in the the query				
			if(!_.contains(modelKeys, field.model)) {
				var pattern = "^[" + modelKey + "]+[0-9]*$";
				var rg = new RegExp(pattern, "g");
				var collections = _.filter(modelValues, function (model){
					var all = model.match(rg);
					if(all) {
						return all[0];
					};
				})
				if(collections) {
					_query.models[field.model] = modelKey + collections.length;
				}else {
					_query.models[field.model] = modelKey;
				}
			}
			_query.select[field.model] =  _query.select[field.model] || []
			
			//include field in query select
			if (field.display) {	
				_query.select[field.model].push(field.field);	
			}
			
			//include aggregate in agrregate container 
			if (field.aggregate && field.aggregate !== "None") {
				_query.aggregates[field.model] = _query.aggregates[field.model] || {}
				_query.aggregates[field.model][field.field] = field.aggregate;
			}

		});
		
		_query.joins = _.compact(_.pluck($scope.reportModelChains,["_related_association"]));
		return _query;
	}

	function groupPossibilities(){
		var all = _.map($scope.groupFields, function (field){
			return field.model + "."+ field.field ;
		});

		return all;
	}

	function groupTest() {
		var all = _.map($scope.groupFields, function (field){
			if (field.checked) {
				return field.table_name + "."+ field.field;	
			}
		})
		return all;
	}

	/**
	 * Delegate handler for an association of the current model
	 * @param  {object} assoc
	 * @return {void}
	 */
	$scope.showAssocModelFields = function (assoc){
		var foundModel = _.find($scope.models, function (model){
			return assoc.table_name === model.table_name;
		});

		pushIntoChain(foundModel, assoc.name);
		$scope.selectedModel = foundModel;
		$scope. showProps = true;
	}

	/**
	 * Move a field up 
	 * @param  {integer} index
	 * @return {void}
	 */
	$scope.moveRfUp = function (index){
		if(index > 0){
			var tempIdx = index - 1;
			var temp = $scope.reportFields[tempIdx];
			$scope.reportFields[tempIdx] = $scope.reportFields[index];
			$scope.reportFields[index] =  temp;
		}
	}

	$scope.moveRfDown = function (index){
		if(index < $scope.reportFields.length - 1){
			var tempIdx = index + 1;
			var temp = $scope.reportFields[tempIdx];
			$scope.reportFields[tempIdx] = $scope.reportFields[index];
			$scope.reportFields[index] =  temp;
		}
	}

	$scope.executeQuery = function (){
		MSG.hide();
		if($scope.query) {
			$scope.query.is_test = true;
			$scope.query.groups.possibilities = _.compact(groupPossibilities());
			$scope.query.groups.test = _.compact(groupTest());

			$scope.runningQuery = true;
			
			Report.runQuery($scope.query).success(function (res){
				$scope.testData["headers"] = res.data.fields;
				$scope.testData["results"] = res.data.results;
				$scope.testData["query"] = res.sql;

				window.res = res;
				$rootScope.$broadcast("repDataChanged");
				$scope.runningQuery = false;
			});
		}else {
			MSG.error("No field selected. You can't send can an empty query to the server.")
		}
	}

	$scope.newReport = function (){
		$scope.show_create_report = true;
		processNewReport();
	}

	function processNewReport (){
		$scope.report = angular.copy(reportDefinition);
	}

	$scope.saveReport = function (){
		MSG.hide()
		console.log($scope.reportFields)
		if ($scope.reportFields.length === 0) {
			console.log("fields are 0")
			$scope.report.query_object = {}
		}else {
			var q = generateQueryStructure();
			console.log(q)
			$scope.report.query_object = q;

			//pull in the raw value
			$scope.report.query_object["raw"] = getRawFields();
		}
		
		var report =  angular.copy($scope.report);
		console.log(report)

		var validation = validate_report();
		if (validation.status) {
			if (report.id) {
				Report.update(report).then(function (res){
					afterSave(res.data);
				});
			}else {
				Report.create(report).then(function (res){
					afterSave(res.data);
				});
			}
		}else {
			MSG.error(validation.message)
		}
	}

	function afterSave(res){
		if (res.success) {
			MSG.success(res.message)
			$scope.report = res.data;
			var found = _.find($scope.reports, function (_report){
				return _report.id === res.data.id
			});

			if (found) {
				//update
				$scope.reports[$scope.reports.indexOf(found)]  = res.data;
			}else {
				//insert
				$scope.reports.push(res.data)
			}
		}else {
			MSG.error(res.message)
		}
	}

	function validate_report(){
		var status = true
		var msg = "";
		if (angular.equals($scope.report.query_object, {}) ){
			status = false;
			msg =  "Cant save report '"+ $scope.report.name +"' becuase no items have been selected."
		}

		if (!$scope.report.name) {
			status = false;
			msg = "Report has no name";
		}
	
		return {
			status: status,
			message : msg
		}
	}

	$scope.openReport = function (){
		//attempt to load reports assuming we haven't already loaded them
		if ($scope.reports.length === 0) {
				Report.all().then(function (res){
					window.rr = res.data;
				if (res.data.success) {
					$scope.reports =  window.reps = res.data.data
				};
			})
		}
		//show the dialog
		$scope.show_open_report = true;
	}

	$scope.processCreateReport = function (){
		if ($scope.report.name) {
			$scope.show_create_report = false;
			MSG.success("Report " + $scope.report.name + " created.");
		}else {
			MSG.error("Report has no name.");
		}
	}

	$scope.processOpenReport = function (){
		console.log($scope.reportToOpen);
		if ($scope.reportToOpen.val) {
			$scope.report = $scope.reportToOpen.val;
			try {
				setFields($scope.reportToOpen.val.query_object.raw)	
				$scope.query = generateQueryStructure();
				_showFields($scope.reportModelChains[0]);
			}catch (e){
				throw e;
			}
			
			$scope.show_open_report = false;
		}
	}

	$scope.clearAllFields = function (){
		$scope.testData["headers"] = [];
		$scope.testData["results"] = [];
		$scope.testData["query"] = "";

		$scope.reportFields  			= [];
		$scope.orderFields 				= [];
		$scope.groupFields 				= [];
		$scope.filterFields 			= [];
		$scope.reportModelChains 	= [];
		$scope.reportModels 			= [];

		$scope.selectedModel = null;

		$scope.query = null;
		$scope.runningQuery = false;

		$scope.models.forEach(function(model){
			model.fields.forEach(function (field){
				field.checked = false;
			});
		});

		$scope.report = {};
		
		$scope.reportModelChains = [];
		window.aa = $scope.reportModelChains
		$rootScope.$broadcast("repDataChanged")
	}

	function getDefaultQuery () {
		return {
			is_test: false,
			select: {},
			aggregates: {},
			models: {},
			where: {},
			groups: {
				possibilities : [],
				test : []
			}
		}
	}

	var setFields = function (raw){
		if (raw) {
			$scope.reportFields  			= raw.reportFields 		
			$scope.filterFields 			= raw.filterFields 		
			$scope.groupFields 				= raw.groupsFields 		
			$scope.orderFields 				= raw.orderFields 		
			$scope.reportModelChains 	= raw.reportModelChains
			$scope.reportModels 			= raw.reportModels 		
		}		
	}

	var getRawFields = window.rawF  = function (){
		return {
			reportFields 							: $scope.reportFields,
			filterFields 							: $scope.filterFields,
			groupsFields							: $scope.groupFields,
			orderFields 							: $scope.orderFields,
			reportModelChains					: $scope.reportModelChains,
			reportModels 							: $scope.reportModels
		}
	}

	function start(){
		getModels();
	}

	start();
	processNewReport();
}
	
var app = angular.module('app');

app.controller("MasterController", function ($scope, $rootScope, $location, $route, $routeParams, $http){
	$scope.navigations = [
		{
			key : "home", 
			value : {
				text : "Dashboard",
				link : "#",
				class: "active",
				children : [
					{
						text: "Home",
						icon_class: 'home',
						id: "home",
						link : "#"
					},
					{
						text: "Reports",
						icon_class: "reports",
						id: "all_reports",
						link : "#all_reports"
					}
				]
			}
		},
		{
			key : "sales",  
			value : {
				text : "Sales",
				link : "#/sales",
				class: "",
				children : [
					{
						text: "Sales Point",
						icon_class: "sales",
						id: "sales",
						link : "#sales"
					},
					{
						text: "Sales Reports",
						icon_class: "reports",
						id: "sales_reports",
						link : "#sales_reports"
					}
				]
			}
		},
		{
			key : "purchases_and_orders", 
			value : {
				text : "Orders & Purchases",
				link : "#/purchase_orders",
				class: "",
				children : [
					{
						text: "Purchase Orders",
						icon_class: "purchase_orders",
						id: "purchase_orders",
						link : "#/purchase_orders"
					},
					{
						text: "Purchases",
						icon_class: "purchases",
						id: "purchases",
						link : "#/purchases"
					},
					{
						text: "Reports",
						icon_class: "reports",
						id: "purchases_and_orders_reports",
						link : "#purchases_and_orders_reports"
					}
				]
			}
		},{
			key : "admin",
			value : {
				text: "Management",
				link: "#/management",
				class: "",
				children : [
					{
						text: "Products",
						link : "#/products",
						icon_class: "products",
						id: "products"
					},
					{
						text: "Product Categories",
						link : "#/product_categories",
						icon_class: "categories",
						id: "product_categories"
					},
					{
						text: "Roles",
						link : "#/roles",
						icon_class : "roles",
						id : "roles"
					},
					{
						text: "Users",
						link : "#/users",
						icon_class : "users",
						id: "users"
					},
					{
						text: "Permissions",
						link : "#/permissions",
						icon_class : "permissions",
						id: "permissions"
					},
					{
						text: "Report Creator",
						link : "#/reports_designer",
						icon_class : "reports",
						id: "reports_designer"
					}
				]
			}
		}
	];

	var rootScope = $rootScope;

	function changeNavs(mPage, cPage){
		if(mPage){
			var mainPage = null;
			var nav;
			for(var i=0; i < $scope.navigations.length; i++ ){
				nav = $scope.navigations[i];
				nav.value.class = "";
				if(nav.key === mPage){
					
					mainPage = nav;
					//change the active link as well
					mainPage.value.class = "active";
					if(cPage){
						// find the child page
						mainPage.value.children.forEach(function(child){
							child.class = "";
							
							if (child.id === cPage){
								child.class = "active_link";
							}
						});
					}
					// break;
				}	
			}

			$scope.childNavs = mainPage.value.children;
		}
	}

	$scope.recentOperations = [];
	
	function getRecent(){
		$http.get("api/recent_operations").then(function (res){
			$scope.recentOperations = res.data.data;
		});
	}

	getRecent();

	$rootScope.$on('$routeChangeSuccess', function(evt, cur, prev) {	
		
		var mainPage = $route.current.$$route.mPage;
		var childPage = $route.current.$$route.cPage;
		changeNavs(mainPage, childPage)
	});

	$scope.print = function (url){
		console.log(url);
	}

});
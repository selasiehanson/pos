'use strict';

var app = angular.module('app');

app.controller("UsersController", function ($scope, User){
	$scope.users = [];
	User.query(function(res){
		$scope.users = res.data;
	});

	$scope.rolesToString = function (user_roles){
		var out = "";
		user_roles.forEach(function (user_role){
			out += user_role.role.name + ", ";
		});
		
		var li = out.lastIndexOf(",")
		return out.substr(0,li)
	}
});

app.controller("UsersEditController", function ($scope, $routeParams, $location, User, Role, RolesUser, MSG){
	// todo pull out all the roles of this user and select the one to which he belongs
	$scope.allRoles = [];
	$scope.currentRoles = null;

	var additions = [];
	var removals = [];

	function getUser(id, callback){
		User.get({id: id}, function (res){
			if(res.success){
				$scope.user = res.data
				if(callback) {
					callback(res.data.roles_users)	
				}
			}	
		});
	}

	function sort_roles(user_roles) {
		$scope.currentRoles = user_roles;
		for (var i = 0; i < $scope.allRoles.length; i++) {
			for (var j = 0; j < $scope.currentRoles.length; j++) {
				if ($scope.allRoles[i]["id"] === $scope.currentRoles[j]["role_id"]) {
					// console.log($scope.allRoles[i]["name"])
					additions.push($scope.allRoles[i]["id"]);
					$scope.allRoles[i]["checked"] = true;
				}
			}
		}
	}

	//get the particular user
	if($routeParams.id){
		Role.query(function (res){
			$scope.allRoles = res.data;
			getUser($routeParams.id, sort_roles);
		});
	}

	$scope.toggleChecked = function (role){
		var idx = null;
		if(role.checked){
			//first remove from removals
			idx = _.indexOf(removals,role.id)
			if (idx > -1) {
				removals.splice(idx, 1);
			}; 
			additions.push(role.id)
		}else {
			idx = _.indexOf(additions, role.id)
			console.log(idx)
			if (idx > -1) {
				additions.splice(idx, 1);
			}; 
			removals.push(role.id)
		}
		
	}

	$scope.save = function(){
		var data = {
			additions : additions,
			removals : removals,
			user_id : $scope.user.id
		}
		var userRoles = new RolesUser(data);
		userRoles.$save(function (res){
			if(res.success){
				MSG.success(res.message);
			}else {
				MSG.error(res.message);
			}
		});
	}

});
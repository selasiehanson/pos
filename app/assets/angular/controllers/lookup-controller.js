var app = angular.module("app")

app.controller("LookUpController", function ($scope, LookUp){
	
	LookUp.register("ProductCategory", "product_categories");
	LookUp.register("Role", "roles");

	//this is sent from th incoming embeded partial
	$scope.lookup = {};
	$scope.saving_lookup = false;

	$scope.current_lookup = { }
	var lookups = [
			{
				name: "Product Category",
				model: "ProductCategory",
				url: "partials?partial=_form_detail&model=product_categories"
		}
	]
	
	$scope.$on("dynamic_look_up_creation", function (event , args){
		var all = _.filter(lookups,function (lookup){
		  return lookup.model === args.model
		});
		
		$scope.$apply(function(){
			$scope.current_lookup = all[0];	
		})
		$("#generic_lookup_container").modal({})
	});

	$scope.save =  function(){
		var data = { 
			name: $scope.lookup.name, 
			description: $scope.lookup.description || ""
		}
		
		$scope.saving_lookup = true;
		LookUp.create($scope.current_lookup.model, data, function (res){
			$scope.saving_lookup = false;
		});
	}

});
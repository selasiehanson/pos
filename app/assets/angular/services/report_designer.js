var app  = angular.module("app");
app.service("Report", function ($http){
	return {
		all: function (){
			return $http.get("reports");
		},
		get : function (type){
			var url = "";
			var prefix = "reports";
			
			switch(type){
				case "models":
					url =  prefix + "/models";
				break;
			}
			
			if(url) {
				return $http.get(url);
			}
		},
		post: function (data, _url){
			var url =  this.generateUrl(_url)
			return $http.post(url, data)
		},
		put: function (data, _url){
			var url =  this.generateUrl(_url) + "/" + data.id
			return $http.put(url, data)
		},
		generateUrl: function (_url){
			var url = "reports" + "/";
			if (_url) {
				url =  url + _url;
			}
			return url;
		},
		create : function(report){
			return this.post(report)
		},
		update : function (report){
			return this.put(report)
		},
		models : function (){
			return this.get("models");
		}, 
		runQuery : function (query){
			return this.post( query, "run");
		},
		saveReport: function (){

		} 
	}
});

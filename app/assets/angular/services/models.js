'use strict';
var app = angular.module("models",['ngResource'])

// app.factory("Report", function ($resource){
// 	return $resource('api/reports/:id', {id : '@id'}, {	    
// 		query: { method: 'GET', isArray: false},
// 	    update : { method : 'PUT'}
// 	});
// });

app.factory("User", function ($resource){
	return $resource('api/users/:id', {id : '@id'}, {	    
		query: { method: 'GET', isArray: false},
	    update : { method : 'PUT'}
	});
});

app.factory("Role", function ($resource){
	return $resource('api/roles/:id', {id : '@id'}, {	    
		query: { method: 'GET', isArray: false},
	    update : { method : 'PUT'}
	});
});

app.factory("RolesUser", function ($resource){
	return $resource("api/user_roles", { id: '@id'},{
		query : { method : "GET",isArray : false},
		update : { method: "PUT"}
	});
});

app.factory("Permission", function ($resource){
	return $resource('api/permissions/:id', {id : '@id'}, {	    
		query: { method: 'GET', isArray: false},
	    update : { method : 'PUT'}

	});
});

app.factory("RolePermission", function ($resource){
	return $resource('api/roles/:role_id/permissions_roles/:id', {id : '@id', role_id: "@role_id"}, {	    
		query: { method: 'GET', isArray: false},
	    update : { method : 'PUT'}

	});
});

app.factory("Product", function ($resource){
	return $resource('api/products/:id', {id : '@id'}, {	    
		query: { method: 'GET', isArray: false},
	    update : { method : 'PUT'}

	});
});

app.factory("ProductCategory", function ($resource){
	return $resource('api/product_categories/:id', {id : '@id'}, {	    
		query: { method: 'GET', isArray: false},
	  update : { method : 'PUT'}
	});
});

app.factory("PurchaseOrder",function ($resource){
	return $resource('api/purchase_orders/:id', {id : '@id'}, {	    
		query: { method: 'GET', isArray: false},
	    update : { method : 'PUT'}
	});
});

app.factory("Purchase",function ($resource){
	return $resource('api/purchases/:id', {id : '@id'}, {	    
		query: { method: 'GET', isArray: false},
	    update : { method : 'PUT'}
	});
});

app.factory("Sale",function ($resource){
	return $resource('api/sales/:id', {id : '@id'}, {	    
		query: { method: 'GET', isArray: false},
	    update : { method : 'PUT'}
	});
});